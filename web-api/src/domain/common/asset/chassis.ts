import { Asset } from './asset';

export class Chassis extends Asset {
  plateNo: string;
  chassisNo: string;
  axleQuantity: number;
}