export class AssetClassification {
  id: string;
  startDate: Date;
  endDate?: Date;
  assetId: string;
  categoryId: string;
}

/**
 * Commonly used category ids used to classify {@link Asset} objects.
 * @remarks feel free to add more types as more asset classifications are added
 */
export enum AssetTypeCategoryId {
  Container = 'd9971059-4235-450e-8aad-f6195602557b',
  Tractor = 'f9858b03-730f-4a59-a536-347bb2331693',
  Chassis = 'bcfe6eed-e5e6-4f71-b877-b1639a53e00e',
}