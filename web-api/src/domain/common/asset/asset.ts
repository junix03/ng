import { AssetClassification } from './asset-classification';
import { AssetStatus } from './asset-status';
import * as uuid from 'uuid/v4';

/**
 * Represents an asset in the domain.
 */
export class Asset {
  id: string;
  name: string;
  description: string;
  ownerPartyId: string;
  classifications: AssetClassification[];
  statuses: AssetStatus[];

  /**
   * Returns the current status of this {@link Asset} object.
   */
  get currentStatus(): AssetStatus {
    return this.statuses.find(s => s.endDate === null);
  }

  /**
   * Updates the classification of this {@link Asset} object.
   * @param categoryId category of the classification
   */
  updateClassification(categoryId: string) {
    // Check previous classification
    if (this.classifications.length > 0) {
      const previousClassification = this.classifications.find(s => s.endDate === null);
      // Guard same category id
      if (previousClassification.categoryId === categoryId) {
        return;
      }
      previousClassification.endDate = new Date();
    }

    // Create the classification record
    const assetClassification = new AssetClassification();

    assetClassification.id = uuid();
    assetClassification.assetId = this.id;
    assetClassification.startDate = new Date();
    assetClassification.endDate = null;
    assetClassification.categoryId = categoryId;

    this.classifications.push(assetClassification);
  }

  /**
   * Updates the status of this {@link Asset} object.
   * @param statusTypeId the status type id
   */
  updateStatus(statusTypeId: string) {
    // Check if has existing status
    if (this.statuses.length > 0) {
      const previousStatus = this.statuses.find(s => s.endDate === null);
      // Guard same status type
      if (previousStatus.statusTypeId === statusTypeId) {
        return;
      }
      previousStatus.endDate = new Date();
    }
    // Create the party status
    const assetStatus = new AssetStatus();

    assetStatus.id = uuid();
    assetStatus.assetId = this.id;
    assetStatus.statusTypeId = statusTypeId;
    assetStatus.startDate = new Date();
    assetStatus.endDate = null;

    this.statuses.push(assetStatus);
  }
}