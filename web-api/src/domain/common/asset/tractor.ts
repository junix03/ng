import { Asset } from './asset';

export class Tractor extends Asset {
  plateNo: string;
  chassisNo: string;
  engineNo: string;
  axleQuantity: number;
  modelYearId: string;
}