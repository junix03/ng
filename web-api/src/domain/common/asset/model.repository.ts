import { Model } from './model';
import { BaseRepository } from '@infrastructure/repository/base.repository';

export abstract class ModelRepository<T extends Model> {
  /**
   * Retrieves all {@link Model} objects in the domain.
   * @remarks Use with caution. May cause heavy loads.
   */
  abstract async findAll(): Promise<T[]>;

  /**
   * Retrieves all {@link Model} objects in the domain with skip and take.
   * @param skip the number of rows to skip
   * @param take the number of items to take
   */
  abstract async findAll(skip: number, take: number): Promise<T[]>;

  /**
   * Retrieves a single {@link Model} object in the domain.
   * @param id the id of the object
   */
  abstract async find(id: string): Promise<T>;

  /**
   * Adds a asset to the domain regardless of its extended type.
   * @param asset the asset to add
   */
  abstract async add(model: T): Promise<T>;

  /**
   * Updates an existing asset domain object regardless of its extended type.
   * @param asset the asset to update
   */
  abstract async update(model: T): Promise<T>;

  /**
   * Removes a asset from the domain given its id.
   * @param assetIds the asset ids to remove
   */
  abstract async remove(... modelIds: string[]): Promise<void>;
}