export class ModelYear {
  id: string;
  name: string;
  description: string;
  modelId: string;
}