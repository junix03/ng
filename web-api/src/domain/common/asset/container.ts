import { Asset } from './asset';

export class Container extends Asset {
  containerCode: string;
}