export class AssetStatus {
  id: string;
  startDate: Date;
  endDate?: Date;
  assetId: string;
  statusTypeId: string;
}