export class Model {
  id: string;
  name: string;
  description: string;
  makerPartyId: string;
}