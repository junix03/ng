import { ModelYear } from './model-year';

export abstract class ModelYearRepository<T extends ModelYear> {
  /**
   * Retrieves all {@link ModelYear} objects in the domain.
   * @remarks Use with caution. May cause heavy loads.
   */
  abstract async findAll(): Promise<T[]>;

  /**
   * Retrieves all {@link ModelYear} objects in the domain with skip and take.
   * @param skip the number of rows to skip
   * @param take the number of items to take
   */
  abstract async findAll(skip: number, take: number): Promise<T[]>;

  /**
   * Retrieves a single {@link ModelYear} object in the domain.
   * @param id the id of the object
   */
  abstract async find(id: string): Promise<T>;

  /**
   * Adds a asset to the domain regardless of its extended type.
   * @param asset the asset to add
   */
  abstract async add(modelYear: T): Promise<T>;

  /**
   * Updates an existing asset domain object regardless of its extended type.
   * @param asset the asset to update
   */
  abstract async update(modelYear: T): Promise<T>;

  /**
   * Removes a asset from the domain given its id.
   * @param assetIds the asset ids to remove
   */
  abstract async remove(... modelYearIds: string[]): Promise<void>;
}