import { Column, Entity, ManyToOne, PrimaryColumn, RelationId } from 'typeorm';
import { PartyEntity } from '@infrastructure/entity/common/party';

@Entity('model')
export class ModelEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column({ length: 54 })
  name: string;
  @Column({ length: 280 })
  description: string;
  @Column()
  @RelationId((m: ModelEntity) => m.makerParty)
  makerPartyId: string;
  @ManyToOne(type => PartyEntity)
  makerParty: PartyEntity;
}