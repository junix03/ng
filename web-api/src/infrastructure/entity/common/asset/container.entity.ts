import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, RelationId } from 'typeorm';
import { AssetEntity } from '@infrastructure/entity/common/asset/asset.entity';

@Entity('container')
export class ContainerEntity {
  @PrimaryColumn('uuid')
  @RelationId((c: ContainerEntity) => c.asset)
  assetId: string;
  @OneToOne(type => AssetEntity)
  @JoinColumn()
  asset: AssetEntity;
  @Column({ unique: true })
  containerCode: string;
}