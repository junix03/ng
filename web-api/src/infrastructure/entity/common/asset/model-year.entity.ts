import { Column, Entity, ManyToOne, PrimaryColumn, RelationId } from 'typeorm';
import { ModelEntity } from './model.entity';

@Entity('model_year')
export class ModelYearEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column({ length: 54 })
  name: string;
  @Column({ length: 280 })
  description: string;
  @Column()
  @RelationId((m: ModelYearEntity) => m.model)
  modelId: string;
  @ManyToOne(type => ModelEntity)
  model: ModelEntity;
}