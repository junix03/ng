import { Column, Entity, ManyToOne, PrimaryColumn, RelationId } from 'typeorm';
import { AssetEntity } from './asset.entity';
import { StatusTypeEntity } from '@infrastructure/entity/system-administration';

@Entity('asset_status')
export class AssetStatusEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column()
  startDate: Date;
  @Column({ nullable: true })
  endDate?: Date;
  @Column()
  @RelationId((a: AssetStatusEntity) => a.asset)
  assetId: string;
  @ManyToOne(type => AssetEntity)
  asset: AssetEntity;
  @Column()
  @RelationId((a: AssetStatusEntity) => a.statusType)
  statusTypeId: string;
  @ManyToOne(type => StatusTypeEntity)
  statusType: StatusTypeEntity;
}