import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn, RelationId } from 'typeorm';
import { AssetEntity } from './asset.entity';
import { ModelYearEntity } from './model-year.entity';

@Entity('tractor')
export class TractorEntity {
  @PrimaryColumn('uuid')
  @RelationId((t: TractorEntity) => t.asset)
  assetId: string;
  @OneToOne(type => AssetEntity)
  @JoinColumn()
  asset: AssetEntity;
  @Column({ unique: true })
  plateNo: string;
  @Column({ unique: true })
  chassisNo: string;
  @Column({ unique: true })
  engineNo: string;
  @Column()
  axleQuantity: number;
  @Column()
  modelYearId: string;
  @ManyToOne(type => ModelYearEntity)
  modelYear: ModelYearEntity;
}