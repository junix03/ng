import { AssetEntity } from './asset.entity';
import { CategoryEntity } from '@infrastructure/entity/common/category';
import { Column, Entity, ManyToOne, PrimaryColumn, RelationId } from 'typeorm';

@Entity('asset_classification')
export class AssetClassificationEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column()
  startDate: Date;
  @Column({ nullable: true })
  endDate?: Date;
  @Column()
  @RelationId((a: AssetClassificationEntity) => a.asset)
  assetId: string;
  @ManyToOne(type => AssetEntity)
  asset: AssetEntity;
  @Column()
  @RelationId((a: AssetClassificationEntity) => a.category)
  categoryId: string;
  @ManyToOne(type => CategoryEntity)
  category: CategoryEntity;
}