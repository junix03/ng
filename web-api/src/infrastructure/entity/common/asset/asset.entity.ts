import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('asset')
export class AssetEntity {
  @PrimaryColumn('uuid')
  id: string;
  @Column({ length: 54 })
  name: string;
  @Column({ length: 280 })
  description: string;
  @Column()
  ownerPartyId: string;
}