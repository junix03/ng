import { Column, Entity, OneToOne, PrimaryColumn, RelationId, JoinColumn } from 'typeorm';
import { AssetEntity } from './asset.entity';

@Entity('chassis')
export class ChassisEntity {
  @PrimaryColumn('uuid')
  @RelationId((c: ChassisEntity) => c.asset)
  assetId: string;
  @OneToOne(type => AssetEntity)
  @JoinColumn()
  asset: AssetEntity;
  @Column({ unique: true })
  plateNo: string;
  @Column({ unique: true })
  chassisNo: string;
  @Column()
  axleQuantity: number;
}