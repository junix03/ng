import { Provider } from '@nestjs/common';
import { ContainerEntityRepository } from './container-entity.repository';
import { TractorEntityRepository } from './tractor-entity.repository';
import { ChassisEntityRepository } from './chassis-entity.repository';
import { ModelEntityRepository } from './model-entity.repository';
import { ModelYearEntityRepository } from './model-year-entity.respository';

// Remember for repositories with subclasses, don't make magic constants for
// injector tokens
export const ASSET_CONTAINER_REPO = 'ContainerRepository';
export const ASSET_TRACTOR_REPO = 'TractorRepository';
export const ASSET_CHASSIS_REPO = 'ChassisRepository';
export const ASSET_MODEL_REPO = 'ModelRepository';
export const ASSET_MODEL_YEAR_REPO = 'ModelYearRepository';

export const assetProviders: Provider[] = [
  {
    provide: ASSET_CONTAINER_REPO,
    useClass: ContainerEntityRepository,
  },
  {
    provide: ASSET_TRACTOR_REPO,
    useClass: TractorEntityRepository,
  },
  {
    provide: ASSET_CHASSIS_REPO,
    useClass: ChassisEntityRepository,
  },
  {
    provide: ASSET_MODEL_REPO,
    useClass: ModelEntityRepository,
  },
  {
    provide: ASSET_MODEL_YEAR_REPO,
    useClass: ModelYearEntityRepository,
  },
];