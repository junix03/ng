import { AssetEntityRepository } from './asset-entity.repository';
import { Tractor } from '@domain/common';
import { AssetEntityMapper } from './asset-entity.mapper';
import { AssetClassificationEntity, AssetEntity, AssetStatusEntity, TractorEntity } from '@infrastructure/entity';

export class TractorEntityRepository extends AssetEntityRepository<Tractor> {

  async update(asset: Tractor): Promise<Tractor> {
    await super.update(asset);
    // Update subclass
    const tractorEntity = AssetEntityMapper.mapToTractorEntity(asset);
    await this.manager.save(tractorEntity);
    return asset;
  }

  async find(id: string): Promise<Tractor> {
    const asset = await super.find(id);
    const tractorEntity = await this.manager.findOne(TractorEntity, id);
    return AssetEntityMapper.mapToTractor(asset, tractorEntity);
  }

  async remove(... assetIds: string[]): Promise<void> {
    // Delete the subclasses first
    await this.manager.delete(TractorEntity, assetIds);
    await super.remove(... assetIds);
  }

  async add(asset: Tractor): Promise<Tractor> {
    // Call super
    await super.add(asset);
    // Subclass save
    const tractorEntity = AssetEntityMapper.mapToTractorEntity(asset);
    await this.manager.insert(TractorEntity, tractorEntity);

    return asset;
  }

  async findAll(skip?: number, take?: number): Promise<Tractor[]> {
    let tractorEntities: TractorEntity[] = [];
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      tractorEntities = await this.manager.createQueryBuilder(TractorEntity, 'tractor')
        .orderBy('tractor.plateNo', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
    } else {
      tractorEntities = await this.manager.find(TractorEntity);
    }
    if (tractorEntities.length > 0) {
      const assetIds = tractorEntities.map(p => p.assetId);
      const assetEntities = await this.manager.createQueryBuilder(AssetEntity, 'asset')
        .where('asset.id IN (:...assetIds)', { assetIds })
        .getMany();
      const assetStatusEntities = await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
        .where('asset_status.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assetClassificationEntities = await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
        .where('asset_classification.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assets = AssetEntityMapper.mapToAssets(assetEntities, assetStatusEntities, assetClassificationEntities);
      return AssetEntityMapper.mapToTractors(assets, tractorEntities);
    }
    return [];
  }
}