import { Asset, AssetRepository } from '@domain/common';
import { AssetEntityMapper } from './asset-entity.mapper';
import { AssetClassificationEntity, AssetEntity, AssetStatusEntity } from '@infrastructure/entity';
import { BaseRepository } from '@infrastructure/repository/base.repository';

export abstract class AssetEntityRepository<T extends Asset> extends BaseRepository implements AssetRepository<T> {

  async add(asset: T): Promise<T> {
    // Save root
    const assetEntity = AssetEntityMapper.mapToAssetEntity(asset);
    await this.manager.insert(AssetEntity, assetEntity);
    // Save statuses
    const assetStatusEntities = asset.statuses
      .map(s => AssetEntityMapper.mapToAssetStatusEntity(s));
    await this.manager.save(assetStatusEntities);
    // Save classification
    const assetClassificationEntities = asset.classifications
      .map(s => AssetEntityMapper.mapToAssetClassificationEntity(s));
    await this.manager.save(assetClassificationEntities);

    return asset as T;
  }

  async find(id: string): Promise<T> {
    const assetEntity = await this.manager.findOne(AssetEntity, id);
    const assetStatusEntities = await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
      .where('asset_status.assetId = :id', { id })
      .getMany();
    const assetClassificationEntities = await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
      .where('asset_classification.assetId  = :id', { id })
      .getMany();
    return AssetEntityMapper.mapToAsset(assetEntity, assetStatusEntities, assetClassificationEntities) as T;

  }

  async findAll(): Promise<T[]>;
  async findAll(skip: number, take: number): Promise<T[]>;
  async findAll(skip?: number, take?: number): Promise<T[]> {
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      const assetEntities = await this.manager.createQueryBuilder(AssetEntity, 'asset')
        .orderBy('asset.name', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
      const assetIds = assetEntities.map(p => p.id);
      const assetStatusEntities = await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
        .where('asset_status.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assetClassificationEntities = await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
        .where('asset_classification.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      return AssetEntityMapper.mapToAssets(assetEntities, assetStatusEntities, assetClassificationEntities) as T[];
    } else {
      // Do retrieve everything
      const assetEntities = await this.manager.find(AssetEntity);
      // Retrieve statuses
      const assetStatusEntities = await this.manager.find(AssetStatusEntity);
      const assetClassificationEntities = await this.manager.find(AssetClassificationEntity);
      // Assemble the aggregate
      return AssetEntityMapper.mapToAssets(assetEntities, assetStatusEntities, assetClassificationEntities) as T[];
    }
  }

  async remove(... assetIds: string[]): Promise<void> {
    // Delete statuses before root first
    await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
      .delete()
      .where('assetId in (:...assetIds)', { assetIds })
      .execute();
    // Then delete classifications
    await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
      .delete()
      .where('assetId in (:...assetIds)', { assetIds })
      .execute();
    // Delete root
    await this.manager.delete(AssetEntity, assetIds);
  }

  async update(asset: T): Promise<T> {
    // Update asset first
    const assetEntity = AssetEntityMapper.mapToAssetEntity(asset);
    await this.manager.save(assetEntity);
    // Update the statuses as well
    const assetStatusEntities = asset.statuses
      .map(s => AssetEntityMapper.mapToAssetStatusEntity(s));
    await this.manager.save(assetStatusEntities);
    // Update classifications as well
    const assetClassificationEntities = asset.classifications
      .map(c => AssetEntityMapper.mapToAssetClassificationEntity(c));
    await this.manager.save(assetClassificationEntities);
    return asset;
  }

}