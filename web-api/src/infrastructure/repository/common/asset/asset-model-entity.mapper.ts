import { ModelEntity } from '@infrastructure/entity';
import { Model } from '@domain/common';

export class AssetModelEntityMapper {

 static mapToModels(modelEntities: ModelEntity[]): Model[] {
    const models: Model[] = [];

    for (const modelEntity of modelEntities) {
      if (!!modelEntity) {
        const model = this.mapToModel(modelEntity);
        models.push(model);
      }
    }
    return models;
  }

  static mapToModel(modelEntity: ModelEntity): Model {
    const model = new Model();
    model.id = modelEntity.id;
    model.name = modelEntity.name;
    model.description = modelEntity.description;
    model.makerPartyId = modelEntity.makerPartyId;

    return model;
  }

  static mapToModelEntity(model: Model): ModelEntity {
    const modelEntity = new ModelEntity();
    modelEntity.id = model.id;
    modelEntity.name = model.name;
    modelEntity.description = model.description;
    modelEntity.makerPartyId = model.makerPartyId;
    return modelEntity;
  }

}