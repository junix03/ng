import { ModelYear, ModelYearRepository } from '@domain/common';
import { ModelYearEntity } from '@infrastructure/entity';
import { BaseRepository } from '@infrastructure/repository/base.repository';
import { AssetModelYearEntityMapper } from './asset-model-year-entity.mapper';

export abstract class AssetModelYearEntityRepository<T extends ModelYear> extends BaseRepository implements ModelYearRepository<T> {

  async add(modelYear: T): Promise<T> {
    // Save root
    const modelYearEntity = AssetModelYearEntityMapper.mapToModelYearEntity(modelYear);
    await this.manager.insert(ModelYearEntity, modelYearEntity);

    return modelYear as T;
  }

  async find(id: string): Promise<T> {
    const modelYearEntity = await this.manager.findOne(ModelYearEntity, id);

    return AssetModelYearEntityMapper.mapToModelYear(modelYearEntity) as T;

  }

  async findAll(): Promise<T[]>;
  async findAll(skip: number, take: number): Promise<T[]>;
  async findAll(skip?: number, take?: number): Promise<T[]> {
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      const modelYearEntities = await this.manager.createQueryBuilder(ModelYearEntity, 'modelYear')
        .orderBy('modelYear.name', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();

      return AssetModelYearEntityMapper.mapToModelYears(modelYearEntities) as T[];
    } else {
      // Do retrieve everything
      const modelYearEntities = await this.manager.find(ModelYearEntity);

      return AssetModelYearEntityMapper.mapToModelYears(modelYearEntities) as T[];
    }
  }

  async remove(... modelYearIds: string[]): Promise<void> {
    await this.manager.delete(ModelYearEntity, modelYearIds);
  }

  async update(modelYear: T): Promise<T> {
    // Update asset first
    const modelYearEntity = AssetModelYearEntityMapper.mapToModelYearEntity(modelYear);
    await this.manager.save(modelYearEntity);
    return modelYear;
  }

}