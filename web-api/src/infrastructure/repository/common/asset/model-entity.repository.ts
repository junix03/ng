import { Model } from '@domain/common/asset';
import { ModelEntity } from '@infrastructure/entity/common';
import { AssetModelEntityRepository } from './asset-model-entity.repository';
import { AssetModelEntityMapper } from './asset-model-entity.mapper';

export class ModelEntityRepository extends AssetModelEntityRepository<Model>  {

  async findAll(): Promise<Model[]>;
  async findAll(skip: number, take: number): Promise<Model[]>;
  async findAll(skip?: number, take?: number): Promise<Model[]> {
    let modelEntities: ModelEntity[] = [];
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      modelEntities = await this.manager.createQueryBuilder(ModelEntity, 'model')
        .orderBy('model.name', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
    } else {
      modelEntities = await this.manager.find(ModelEntity);
    }
    if (modelEntities.length > 0) {
      return AssetModelEntityMapper.mapToModels(modelEntities);
    }
    return [];
  }

  async find(id: string): Promise<Model> {
    const modelEntity = await this.manager.findOne(ModelEntity, id);
    return AssetModelEntityMapper.mapToModel(modelEntity);
  }

  async update(model: Model): Promise<Model> {
    await this.revise(model);
    // revise model
    const modelEntity = AssetModelEntityMapper.mapToModelEntity(model);
    await this.manager.save(modelEntity);
    return model;
  }

  async remove(... modelIds): Promise<void> {
    // Delete model
    await this.manager.delete(ModelEntity, modelIds);
    await this.delete(... modelIds);
  }

  async add(model: Model): Promise<Model> {
    // save
     const modelEntity = AssetModelEntityMapper.mapToModelEntity(model);
     await this.manager.insert(ModelEntity, modelEntity);

     return model;
  }

 async revise(model: Model): Promise<Model> {
    // revise model
    const modelEntity = AssetModelEntityMapper.mapToModelEntity(model);
    await this.manager.save(modelEntity);

    return model;
  }

  async delete(... modelIds: string[]): Promise<void> {
    // Delete model
    await this.manager.delete(ModelEntity, modelIds);
  }
}