import { ModelYear } from '@domain/common';
import { ModelYearEntity } from '@infrastructure/entity';

export class AssetModelYearEntityMapper {

 static mapToModelYears(modelYearEntities: ModelYearEntity[]): ModelYear[] {
    const modelYears: ModelYear[] = [];

    for (const modelYearEntity of modelYearEntities) {
      if (!!modelYearEntity) {
        const modelYear = this.mapToModelYear(modelYearEntity);
        modelYears.push(modelYear);
      }
    }
    return modelYears;
  }

 static mapToModelYear(modelYearEntity: ModelYearEntity): ModelYear {
    const modelYear = new ModelYear();
    modelYear.id = modelYearEntity.id;
    modelYear.name = modelYearEntity.name;
    modelYear.description = modelYearEntity.description;
    modelYear.modelId = modelYearEntity.modelId;

    return modelYear;
  }

 static mapToModelYearEntity(modelYear: ModelYear): ModelYearEntity {
    const modelYearEntity = new ModelYearEntity();
    modelYearEntity.id = modelYear.id;
    modelYearEntity.name = modelYear.name;
    modelYearEntity.description = modelYear.description;
    modelYearEntity.modelId = modelYear.modelId;
    return modelYearEntity;
  }

}