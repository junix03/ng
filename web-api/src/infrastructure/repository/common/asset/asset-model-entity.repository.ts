import { Model, ModelRepository } from '@domain/common';
import { ModelEntity } from '@infrastructure/entity';
import { BaseRepository } from '@infrastructure/repository/base.repository';
import { AssetModelEntityMapper } from './asset-model-entity.mapper';

export abstract class AssetModelEntityRepository<T extends Model> extends BaseRepository implements ModelRepository<T> {

  async add(model: T): Promise<T> {
    // Save root
    const modelEntity = AssetModelEntityMapper.mapToModelEntity(model);
    await this.manager.insert(ModelEntity, modelEntity);

    return model as T;
  }

  async find(id: string): Promise<T> {
    const modelEntity = await this.manager.findOne(ModelEntity, id);

    return AssetModelEntityMapper.mapToModel(modelEntity) as T;

  }

  async findAll(): Promise<T[]>;
  async findAll(skip: number, take: number): Promise<T[]>;
  async findAll(skip?: number, take?: number): Promise<T[]> {
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      const modelEntities = await this.manager.createQueryBuilder(ModelEntity, 'model')
        .orderBy('model.name', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();

      return AssetModelEntityMapper.mapToModels(modelEntities) as T[];
    } else {
      // Do retrieve everything
      const modelEntities = await this.manager.find(ModelEntity);

      return AssetModelEntityMapper.mapToModels(modelEntities) as T[];
    }
  }

  async remove(... modelIds: string[]): Promise<void> {
    await this.manager.delete(ModelEntity, modelIds);
  }

  async update(model: T): Promise<T> {
    // Update asset first
    const modelEntity = AssetModelEntityMapper.mapToModelEntity(model);
    await this.manager.save(modelEntity);
    return model;
  }

}