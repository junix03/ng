import { Asset, AssetClassification, AssetStatus, Container, Tractor, Chassis } from '@domain/common';
import { AssetClassificationEntity, AssetEntity, AssetStatusEntity, ContainerEntity, TractorEntity, ChassisEntity } from '@infrastructure/entity';

/**
 * Utility class to map {@link Asset} objects to {@link AssetEntity} and vice
 * versa.
 */
export class AssetEntityMapper {
  static mapToAssetEntity(asset: Asset) {
    const assetEntity = new AssetEntity();

    assetEntity.id = asset.id;
    assetEntity.name = asset.name;
    assetEntity.description = asset.description;
    assetEntity.ownerPartyId = asset.ownerPartyId;

    return assetEntity;
  }

  static mapToAssetStatusEntity(status: AssetStatus) {
    const assetStatusEntity = new AssetStatusEntity();

    assetStatusEntity.id = status.id;
    assetStatusEntity.startDate = status.startDate;
    assetStatusEntity.endDate = status.endDate;
    assetStatusEntity.assetId = status.assetId;
    assetStatusEntity.statusTypeId = status.statusTypeId;

    return assetStatusEntity;
  }

  static mapToAssetClassificationEntity(assetClassification: AssetClassification) {
    const assetClassificationEntity = new AssetClassificationEntity();

    assetClassificationEntity.id = assetClassification.id;
    assetClassificationEntity.categoryId = assetClassification.categoryId;
    assetClassificationEntity.endDate = assetClassification.endDate;
    assetClassificationEntity.startDate = assetClassification.startDate;
    assetClassificationEntity.assetId = assetClassification.assetId;

    return assetClassificationEntity;
  }

  static mapToAssets(assetEntities: AssetEntity[],
                     assetStatusEntities: AssetStatusEntity[],
                     assetClassificationEntities: AssetClassificationEntity[]) {
    return assetEntities.map(assetEntity =>
      this.mapToAsset(assetEntity, assetStatusEntities, assetClassificationEntities));
  }

  static mapToAsset(assetEntity: AssetEntity,
                    assetStatusEntities: AssetStatusEntity[],
                    assetClassificationEntities: AssetClassificationEntity[]) {
    const asset = new Asset();
    asset.id = assetEntity.id;
    asset.name = assetEntity.name;
    asset.description = assetEntity.description;
    asset.ownerPartyId = assetEntity.ownerPartyId;
    asset.statuses = assetStatusEntities
      .filter(p => p.assetId === assetEntity.id)
      .map(p => this.mapToAssetStatus(p));
    asset.classifications = assetClassificationEntities
      .filter(p => p.assetId === assetEntity.id)
      .map(p => this.mapToAssetClassification(p));

    return asset;
  }

  static mapToAssetStatus(assetStatusEntity: AssetStatusEntity) {
    const assetStatus = new AssetStatus();
    assetStatus.id = assetStatusEntity.id;
    assetStatus.statusTypeId = assetStatusEntity.statusTypeId;
    assetStatus.assetId = assetStatusEntity.assetId;
    assetStatus.endDate = assetStatusEntity.endDate;
    assetStatus.startDate = assetStatusEntity.startDate;
    return assetStatus;
  }

  static mapToAssetClassification(assetClassificationEntity: AssetClassificationEntity) {
    const assetClassification = new AssetClassification();
    assetClassification.id = assetClassificationEntity.id;
    assetClassification.categoryId = assetClassificationEntity.categoryId;
    assetClassification.assetId = assetClassificationEntity.assetId;
    assetClassification.endDate = assetClassificationEntity.endDate;
    assetClassification.startDate = assetClassificationEntity.startDate;

    return assetClassification;
  }

  static mapToContainerEntity(container: Container): ContainerEntity {
    const containerEntity = new ContainerEntity();

    containerEntity.assetId = container.id;
    containerEntity.containerCode = container.containerCode;

    return containerEntity;
  }

  static mapToTractorEntity(tractor: Tractor): TractorEntity {
    const tractorEntity = new TractorEntity();

    tractorEntity.assetId = tractor.id;
    tractorEntity.plateNo = tractor.plateNo;
    tractorEntity.chassisNo = tractor.chassisNo;
    tractorEntity.engineNo = tractor.engineNo;
    tractorEntity.axleQuantity = tractor.axleQuantity;
    tractorEntity.modelYearId = tractor.modelYearId;

    return tractorEntity;
  }

  static mapToChassisEntity(chassis: Chassis): ChassisEntity {
    const chassisEntity = new ChassisEntity();

    chassisEntity.assetId = chassis.id;
    chassisEntity.plateNo = chassis.plateNo;
    chassisEntity.chassisNo = chassis.chassisNo;
    chassisEntity.axleQuantity = chassis.axleQuantity;

    return chassisEntity;
  }

  static mapToContainer(asset: Asset, containerEntity: ContainerEntity): Container {
    const container = new Container();

    container.id = asset.id;
    container.name = asset.name;
    container.description = asset.description;
    container.ownerPartyId = asset.ownerPartyId;
    container.classifications = asset.classifications;
    container.statuses = asset.statuses;
    container.containerCode = containerEntity.containerCode;

    return container;
  }

  static mapToTractor(asset: Asset, tractorEntity: TractorEntity): Tractor {
    const tractor = new Tractor();

    tractor.id = asset.id;
    tractor.name = asset.name;
    tractor.description = asset.description;
    tractor.ownerPartyId = asset.ownerPartyId;
    tractor.classifications = asset.classifications;
    tractor.statuses = asset.statuses;
    tractor.plateNo = tractorEntity.plateNo;
    tractor.chassisNo = tractorEntity.chassisNo;
    tractor.engineNo = tractorEntity.engineNo;
    tractor.axleQuantity = tractorEntity.axleQuantity;
    tractor.modelYearId = tractorEntity.modelYearId;

    return tractor;
  }

  static mapToChassis(asset: Asset, chassisEntity: ChassisEntity): Chassis {
    const chassis = new Chassis();

    chassis.id = asset.id;
    chassis.name = asset.name;
    chassis.description = asset.description;
    chassis.ownerPartyId = asset.ownerPartyId;
    chassis.classifications = asset.classifications;
    chassis.statuses = asset.statuses;
    chassis.plateNo = chassisEntity.plateNo;
    chassis.chassisNo = chassisEntity.chassisNo;
    chassis.axleQuantity = chassisEntity.axleQuantity;

    return chassis;
  }

  static mapToContainers(assets: Asset[], containerEntities: ContainerEntity[]) {
    const containers: Container[] = [];

    for (const asset of assets) {
      const containerEntity = containerEntities.find(p => p.assetId === asset.id);
      if (!!containerEntity) {
        const container = this.mapToContainer(asset, containerEntity);
        containers.push(container);
      }
    }

    return containers;
  }

  static mapToTractors(assets: Asset[], tractorEntities: TractorEntity[]) {
    const tractors: Tractor[] = [];

    for (const asset of assets) {
      const tractorEntity = tractorEntities.find(p => p.assetId === asset.id);
      if (!!tractorEntity) {
        const tractor = this.mapToTractor(asset, tractorEntity);
        tractors.push(tractor);
      }
    }

    return tractors;
  }

  static mapToChasseez(assets: Asset[], chassisEntities: ChassisEntity[]) {
    const chasseez: Chassis[] = [];

    for (const asset of assets) {
      const chassisEntity = chassisEntities.find(p => p.assetId === asset.id);
      if (!!chassisEntity) {
        const chassis = this.mapToChassis(asset, chassisEntity);
        chasseez.push(chassis);
      }
    }

    return chasseez;
  }
}