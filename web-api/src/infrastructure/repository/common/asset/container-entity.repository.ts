import { AssetEntityRepository } from './asset-entity.repository';
import { Container } from '@domain/common';
import { AssetEntityMapper } from './asset-entity.mapper';
import { AssetClassificationEntity, AssetEntity, AssetStatusEntity, ContainerEntity } from '@infrastructure/entity';

export class ContainerEntityRepository extends AssetEntityRepository<Container> {

  async update(asset: Container): Promise<Container> {
    await super.update(asset);
    // Update subclass
    const containerEntity = AssetEntityMapper.mapToContainerEntity(asset);
    await this.manager.save(containerEntity);
    return asset;
  }

  async find(id: string): Promise<Container> {
    const asset = await super.find(id);
    const containerEntity = await this.manager.findOne(ContainerEntity, id);
    return AssetEntityMapper.mapToContainer(asset, containerEntity);
  }

  async remove(... assetIds: string[]): Promise<void> {
    // Delete the subclasses first
    await this.manager.delete(ContainerEntity, assetIds);
    await super.remove(... assetIds);
  }

  async add(asset: Container): Promise<Container> {
    // Call super
    await super.add(asset);
    // Subclass save
    const containerEntity = AssetEntityMapper.mapToContainerEntity(asset);
    await this.manager.insert(ContainerEntity, containerEntity);

    return asset;
  }

  async findAll(skip?: number, take?: number): Promise<Container[]> {
    let containerEntities: ContainerEntity[] = [];
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      containerEntities = await this.manager.createQueryBuilder(ContainerEntity, 'container')
        .orderBy('container.containerCode', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
    } else {
      containerEntities = await this.manager.find(ContainerEntity);
    }
    if (containerEntities.length > 0) {
      const assetIds = containerEntities.map(p => p.assetId);
      const assetEntities = await this.manager.createQueryBuilder(AssetEntity, 'asset')
        .where('asset.id IN (:...assetIds)', { assetIds })
        .getMany();
      const assetStatusEntities = await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
        .where('asset_status.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assetClassificationEntities = await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
        .where('asset_classification.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assets = AssetEntityMapper.mapToAssets(assetEntities, assetStatusEntities, assetClassificationEntities);
      return AssetEntityMapper.mapToContainers(assets, containerEntities);
    }
    return [];
  }
}