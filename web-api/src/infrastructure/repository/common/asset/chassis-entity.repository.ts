import { AssetEntityRepository } from './asset-entity.repository';
import { Chassis } from '@domain/common';
import { AssetEntityMapper } from './asset-entity.mapper';
import { AssetClassificationEntity, AssetEntity, AssetStatusEntity, ChassisEntity } from '@infrastructure/entity';

export class ChassisEntityRepository extends AssetEntityRepository<Chassis> {

  async update(asset: Chassis): Promise<Chassis> {
    await super.update(asset);
    // Update subclass
    const chassisEntity = AssetEntityMapper.mapToChassisEntity(asset);
    await this.manager.save(chassisEntity);
    return asset;
  }

  async find(id: string): Promise<Chassis> {
    const asset = await super.find(id);
    const chassisEntity = await this.manager.findOne(ChassisEntity, id);
    return AssetEntityMapper.mapToChassis(asset, chassisEntity);
  }

  async remove(... assetIds: string[]): Promise<void> {
    // Delete the subclasses first
    await this.manager.delete(ChassisEntity, assetIds);
    await super.remove(... assetIds);
  }

  async add(asset: Chassis): Promise<Chassis> {
    // Call super
    await super.add(asset);
    // Subclass save
    const chassisEntity = AssetEntityMapper.mapToChassisEntity(asset);
    await this.manager.insert(ChassisEntity, chassisEntity);

    return asset;
  }

  async findAll(skip?: number, take?: number): Promise<Chassis[]> {
    let chassisEntities: ChassisEntity[] = [];
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      chassisEntities = await this.manager.createQueryBuilder(ChassisEntity, 'chassis')
        .orderBy('chassis.chassisNo', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
    } else {
      chassisEntities = await this.manager.find(ChassisEntity);
    }
    if (chassisEntities.length > 0) {
      const assetIds = chassisEntities.map(p => p.assetId);
      const assetEntities = await this.manager.createQueryBuilder(AssetEntity, 'asset')
        .where('asset.id IN (:...assetIds)', { assetIds })
        .getMany();
      const assetStatusEntities = await this.manager.createQueryBuilder(AssetStatusEntity, 'asset_status')
        .where('asset_status.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assetClassificationEntities = await this.manager.createQueryBuilder(AssetClassificationEntity, 'asset_classification')
        .where('asset_classification.assetId IN (:...assetIds)', { assetIds })
        .getMany();
      const assets = AssetEntityMapper.mapToAssets(assetEntities, assetStatusEntities, assetClassificationEntities);
      return AssetEntityMapper.mapToChasseez(assets, chassisEntities);
    }
    return [];
  }
}