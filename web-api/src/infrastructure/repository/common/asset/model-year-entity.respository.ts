import { ModelYear } from '@domain/common';
import { ModelYearEntity } from '@infrastructure/entity/common';
import { AssetModelYearEntityRepository } from './asset-model-year-entity.repository';
import { AssetModelYearEntityMapper } from './asset-model-year-entity.mapper';

export  class ModelYearEntityRepository extends AssetModelYearEntityRepository<ModelYear> {

  async findAll(): Promise<ModelYear[]>;
  async findAll(skip: number, take: number): Promise<ModelYear[]>;
  async findAll(skip?: number, take?: number): Promise<ModelYear[]> {
    let modelYearEntities: ModelYearEntity[] = [];
    // Check if skip and take is not null.
    if (skip !== undefined && take !== undefined) {
      // Do skip and take queries (infinite scroll)
      modelYearEntities = await this.manager.createQueryBuilder(ModelYearEntity, 'modelYear')
        .orderBy('modelYear.name', 'ASC')
        .skip(skip)
        .take(take)
        .getMany();
    } else {
      modelYearEntities = await this.manager.find(ModelYearEntity);
    }
    if (modelYearEntities.length > 0) {
      return AssetModelYearEntityMapper.mapToModelYears(modelYearEntities);
    }
    return [];
  }

  async find(id: string): Promise<ModelYear> {
    const modelYearEntity = await this.manager.findOne(ModelYearEntity, id);
    return AssetModelYearEntityMapper.mapToModelYear(modelYearEntity);
  }

  async update(modelYear: ModelYear): Promise<ModelYear> {
    await this.revise(modelYear);
    // revise model
    const modelYearEntity = AssetModelYearEntityMapper.mapToModelYearEntity(modelYear);
    await this.manager.save(modelYearEntity);
    return modelYear;
  }

  async remove(... modelYearIds): Promise<void> {
    // Delete model
    await this.manager.delete(ModelYearEntity, modelYearIds);
    await this.delete(... modelYearIds);
  }

  async add(modelYear: ModelYear): Promise<ModelYear> {
     // save
     const modelYearEntity = AssetModelYearEntityMapper.mapToModelYearEntity(modelYear);
     await this.manager.insert(ModelYearEntity, modelYearEntity);

     return modelYear;
  }

  async revise(modelYear: ModelYear): Promise<ModelYear> {
    // revise model
    const modelYearEntity = AssetModelYearEntityMapper.mapToModelYearEntity(modelYear);
    await this.manager.save(modelYearEntity);

    return modelYear;
  }

  async delete(... modelYearIds: string[]): Promise<void> {
    // Delete model
    await this.manager.delete(ModelYearEntity, modelYearIds);
  }
}