import { EntityManager, getManager } from 'typeorm';
import { createNamespace, getNamespace, Namespace } from 'cls-hooked';
import { Propagation } from './propagation';

export const NAMESPACE_NAME = '__typeOrm___cls_hooked_tx_namespace';
const TYPE_ORM_KEY_PREFIX = '__typeOrm__transactionalEntityManager_';

export const initializeTransactionalContext = () =>
  getNamespace(NAMESPACE_NAME) || createNamespace(NAMESPACE_NAME);

export const getEntityManagerForConnection = (
  connectionName: string,
  context: Namespace,
): EntityManager => {
  return context.get(`${TYPE_ORM_KEY_PREFIX}${connectionName}`);
};

export const setEntityManagerForConnection = (
  connectionName: string,
  context: Namespace,
  entityManager: EntityManager | null,
) => context.set(`${TYPE_ORM_KEY_PREFIX}${connectionName}`, entityManager);

/**
 * Used to declare a Transaction operation.
 * In order to use it, you must use {@link BaseRepository} custom repository in order to use the Transactional decorator
 */
export function Transactional(options?: {
  connectionName?: string
  propagation?: Propagation,
}): MethodDecorator {
  const connectionName: string = options ? options.connectionName ? options.connectionName : 'default' : 'default';
  const propagation: Propagation = options ? options.propagation ? options.propagation : Propagation.REQUIRED : Propagation.REQUIRED;

  return (target: any, methodName: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
    const originalMethod = descriptor.value;

    descriptor.value = function(... args: any[]) {
      const context = getNamespace(NAMESPACE_NAME);
      if (!context) {
        throw new Error(
          'No CLS namespace defined in your app ... please call initializeTransactionalContext() before application start.',
        );
      }

      const runOriginal = async () => originalMethod.apply(this, [... args]);
      const runWithNewTransaction = async () =>
        getManager(connectionName).transaction(async entityManager => {
          setEntityManagerForConnection(connectionName, context, entityManager);
          const result = await originalMethod.apply(this, [... args]);
          setEntityManagerForConnection(connectionName, context, null);
          return result;
        });

      return context.runAndReturn(async () => {
        const currentTransaction = getEntityManagerForConnection(connectionName, context);

        switch (propagation) {
          case Propagation.MANDATORY:
            if (!currentTransaction) {
              throw new TransactionalError(
                'No existing transaction found for transaction marked with propagation \'MANDATORY\'',
              );
            }
            return runOriginal();
          case Propagation.NESTED:
            return runWithNewTransaction();
          case Propagation.NEVER:
            if (currentTransaction) {
              throw new TransactionalError(
                'Found an existing transaction, transaction marked with propagation \'NEVER\'',
              );
            }
            return runOriginal();
          case Propagation.NOT_SUPPORTED:
            if (currentTransaction) {
              setEntityManagerForConnection(connectionName, context, null);
              const result = await runOriginal();
              setEntityManagerForConnection(connectionName, context, currentTransaction);
              return result;
            }
            return runOriginal();
          case Propagation.REQUIRED:
            if (currentTransaction) {
              return runOriginal();
            }
            return runWithNewTransaction();
          case Propagation.REQUIRES_NEW:
            return runWithNewTransaction();
          case Propagation.SUPPORTS:
            return runOriginal();
        }
        if (currentTransaction) {
          return runOriginal();
        }
        return runWithNewTransaction();
      });
    };
  };
}

export class TransactionalError extends Error {
  public name = 'TransactionalError';

  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, TransactionalError.prototype);
  }
}