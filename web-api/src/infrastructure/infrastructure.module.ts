import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { assetProviders, partyProviders } from '@infrastructure/repository';

/**
 * Module that represents the entirety of the infrastructure layer.
 * This module should only be imported by {@link AppModule}.
 */
@Module({
  imports: [TypeOrmModule.forRoot()],
  providers: [
    ... partyProviders,
    ... assetProviders,
  ],
  exports: [
    ... partyProviders,
    ... assetProviders,
  ],
})
export class InfrastructureModule {}