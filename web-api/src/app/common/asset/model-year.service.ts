import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ASSET_MODEL_YEAR_REPO } from '@infrastructure/repository';
import { ModelYear, ModelYearRepository } from '@domain/common';
import { IsUUID, Length } from 'class-validator';
import { Transactional } from '@infrastructure/repository/transactional';

/**
 * Data transfer object for this service.
 */
export class ModelYearModel {
  @IsUUID()
  id: string;
  @Length(0, 53)
  name: string;
  @Length(0, 280)
  description: string;
  @IsUUID()
  modelId: string;
}

@Injectable()
export class ModelYearService {
  constructor(
    @Inject(ASSET_MODEL_YEAR_REPO)
    private readonly modelYearRepository: ModelYearRepository<ModelYear>,
  ) {}

  @Transactional()
  async create(modelYearModel: ModelYearModel): Promise<ModelYearModel> {
    // Create the domain object
    const modelYear = new ModelYear();
    // Map the data transfer object
    modelYear.id = modelYearModel.id;
    modelYear.name = modelYearModel.name;
    modelYear.description = modelYearModel.description;
    modelYear.modelId = modelYearModel.modelId;
    // Add to repository
    await this.modelYearRepository.add(modelYear);

    return modelYearModel;
  }

  async find(id: string): Promise<ModelYearModel> {
    const modelYear = await this.modelYearRepository.find(id);

    const modelYearModel = new ModelYearModel();
    modelYearModel.id = modelYear.id;
    modelYearModel.name = modelYear.name;
    modelYearModel.description = modelYear.description;
    modelYearModel.modelId = modelYear.modelId;

    return modelYearModel;
  }

  async findAll(skip: number = 0, take: number = 20): Promise<ModelYearModel[]> {
    const modelYears = await this.modelYearRepository.findAll(skip, take);

    return modelYears.map(c => {
      const modelYearModel = new ModelYearModel();
      modelYearModel.id = c.id;
      modelYearModel.name = c.name;
      modelYearModel.description = c.description;
      modelYearModel.modelId = c.modelId;

      return modelYearModel;
    });
  }

  @Transactional()
  async delete(... modelYearIds: string[]): Promise<void> {
    await this.modelYearRepository.remove(... modelYearIds);
  }

  @Transactional()
  async update(modelYearId: string, modelYearModel: ModelYearModel): Promise<ModelYearModel> {
    const modelYear = await this.modelYearRepository.find(modelYearId);
    // Update
    modelYear.name = modelYearModel.name;
    modelYear.description = modelYearModel.description;
    modelYear.modelId = modelYearModel.modelId;
    // Invoke the update on the repository
    await this.modelYearRepository.update(modelYear);

    return modelYearModel;
  }
}