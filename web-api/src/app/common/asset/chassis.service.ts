import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ASSET_CHASSIS_REPO } from '@infrastructure/repository';
import { AssetRepository, AssetTypeCategoryId, Chassis } from '@domain/common';
import { IsString, IsUUID, Length, IsNumber } from 'class-validator';
import { Transactional } from '@infrastructure/repository/transactional';

/**
 * Data transfer object for this service.
 */
export class ChassisModel {
  @IsUUID()
  id: string;
  @Length(0, 53)
  name: string;
  @Length(0, 280)
  description: string;
  @IsUUID()
  ownerPartyId: string;
  @IsUUID()
  statusTypeId: string;
  @IsString()
  plateNo: string;
  @IsString()
  chassisNo: string;
  @IsNumber()
  axleQuantity: number;
}

@Injectable()
export class ChassisService {
  constructor(
    @Inject(ASSET_CHASSIS_REPO)
    private readonly assetRepository: AssetRepository<Chassis>,
  ) {}

  @Transactional()
  async create(chassisModel: ChassisModel): Promise<ChassisModel> {
    // Create the domain object
    const chassis = new Chassis();
    // Map the data transfer object
    chassis.id = chassisModel.id;
    chassis.name = chassisModel.name;
    chassis.description = chassisModel.description;
    chassis.ownerPartyId = chassisModel.ownerPartyId;
    chassis.plateNo = chassisModel.plateNo;
    chassis.chassisNo = chassisModel.chassisNo;
    chassis.axleQuantity = chassisModel.axleQuantity;
    chassis.statuses = [];
    chassis.classifications = [];
    // Invoke update status
    chassis.updateStatus(chassisModel.statusTypeId);
    // Classify myself as a container
    chassis.updateClassification(AssetTypeCategoryId.Chassis);
    // Add to repository
    await this.assetRepository.add(chassis);

    return chassisModel;
  }

  async find(id: string): Promise<ChassisModel> {
    const chassis = await this.assetRepository.find(id);

    const chassisModel = new ChassisModel();
    chassisModel.id = chassis.id;
    chassisModel.name = chassis.name;
    chassisModel.description = chassis.description;
    chassisModel.ownerPartyId = chassis.ownerPartyId;
    chassisModel.statusTypeId = chassis.currentStatus.statusTypeId;
    chassisModel.plateNo = chassis.plateNo;
    chassisModel.chassisNo = chassis.chassisNo;
    chassisModel.axleQuantity = chassis.axleQuantity;

    return chassisModel;
  }

  async findAll(skip: number = 0, take: number = 20): Promise<ChassisModel[]> {
    const chasseez = await this.assetRepository.findAll(skip, take);

    return chasseez.map(c => {
      const chassisModel = new ChassisModel();
      chassisModel.id = c.id;
      chassisModel.name = c.name;
      chassisModel.description = c.description;
      chassisModel.ownerPartyId = c.ownerPartyId;
      chassisModel.statusTypeId = c.currentStatus.statusTypeId;
      chassisModel.plateNo = c.plateNo;
      chassisModel.chassisNo = c.chassisNo;
      chassisModel.axleQuantity = c.axleQuantity;

      return chassisModel;
    });
  }

  @Transactional()
  async delete(... chassisIds: string[]): Promise<void> {
    await this.assetRepository.remove(... chassisIds);
  }

  @Transactional()
  async update(chassisId: string, chassisModel: ChassisModel): Promise<ChassisModel> {
    const chassis = await this.assetRepository.find(chassisId);
    // Update
    chassis.name = chassisModel.name;
    chassis.description = chassisModel.description;
    chassis.ownerPartyId = chassisModel.ownerPartyId;
    chassis.plateNo = chassisModel.plateNo;
    chassis.chassisNo = chassisModel.chassisNo;
    chassis.axleQuantity = chassisModel.axleQuantity;
    // Change status
    chassis.updateStatus(chassisModel.statusTypeId);
    // Invoke the update on the repository
    await this.assetRepository.update(chassis);

    return chassisModel;
  }
}