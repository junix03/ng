import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ASSET_MODEL_REPO } from '@infrastructure/repository';
import { Model, ModelRepository } from '@domain/common';
import { IsUUID, Length } from 'class-validator';
import { Transactional } from '@infrastructure/repository/transactional';

/**
 * Data transfer object for this service.
 */
export class ModelModel {
  @IsUUID()
  id: string;
  @Length(0, 53)
  name: string;
  @Length(0, 280)
  description: string;
  @IsUUID()
  makerPartyId: string;
}

@Injectable()
export class ModelService {
  constructor(
    @Inject(ASSET_MODEL_REPO)
    private readonly modelRepository: ModelRepository<Model>,
  ) {}

  @Transactional()
  async create(modelModel: ModelModel): Promise<ModelModel> {
    // Create the domain object
    const model = new Model();
    // Map the data transfer object
    model.id = modelModel.id;
    model.name = modelModel.name;
    model.description = modelModel.description;
    model.makerPartyId = modelModel.makerPartyId;
    // Add to repository
    await this.modelRepository.add(model);

    return modelModel;
  }

  async find(id: string): Promise<ModelModel> {
    const model = await this.modelRepository.find(id);

    const modelModel = new ModelModel();
    modelModel.id = model.id;
    modelModel.name = model.name;
    modelModel.description = model.description;
    modelModel.makerPartyId = model.makerPartyId;

    return modelModel;
  }

  async findAll(skip: number = 0, take: number = 20): Promise<ModelModel[]> {
    const models = await this.modelRepository.findAll(skip, take);

    return models.map(c => {
      const modelModel = new ModelModel();
      modelModel.id = c.id;
      modelModel.name = c.name;
      modelModel.description = c.description;
      modelModel.makerPartyId = c.makerPartyId;
      return modelModel;
    });
  }

  @Transactional()
  async delete(... modelIds: string[]): Promise<void> {
    await this.modelRepository.remove(... modelIds);
  }

  @Transactional()
  async update(modelId: string, modelModel: ModelModel): Promise<ModelModel> {
    const model = await this.modelRepository.find(modelId);
    // Update
    model.name = modelModel.name;
    model.description = modelModel.description;
    model.makerPartyId = modelModel.makerPartyId;
    // Invoke the update on the repository
    await this.modelRepository.update(model);

    return modelModel;
  }
}