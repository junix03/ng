import { ContainerService } from './container.service';
import { TractorService } from './tractor.service';
import { ChassisService } from './chassis.service';
import { ModelService } from './model.service';
import { ModelYearService } from './model-year.service';

export const assetServices = [ContainerService, TractorService, ChassisService, ModelService, ModelYearService];
