import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ASSET_TRACTOR_REPO } from '@infrastructure/repository';
import { AssetRepository, AssetTypeCategoryId, Tractor } from '@domain/common';
import { IsString, IsUUID, Length, IsNumber } from 'class-validator';
import { Transactional } from '@infrastructure/repository/transactional';

/**
 * Data transfer object for this service.
 */
export class TractorModel {
  @IsUUID()
  id: string;
  @Length(0, 53)
  name: string;
  @Length(0, 280)
  description: string;
  @IsUUID()
  ownerPartyId: string;
  @IsString()
  plateNo: string;
  @IsString()
  chassisNo: string;
  @IsString()
  engineNo: string;
  @IsNumber()
  axleQuantity: number;
  @IsString()
  modelYearId: string;
  @IsUUID()
  statusTypeId: string;
}

@Injectable()
export class TractorService {
  constructor(
    @Inject(ASSET_TRACTOR_REPO)
    private readonly assetRepository: AssetRepository<Tractor>,
  ) {}

  @Transactional()
  async create(tractorModel: TractorModel): Promise<TractorModel> {
    // Create the domain object
    const tractor = new Tractor();
    // Map the data transfer object
    tractor.id = tractorModel.id;
    tractor.name = tractorModel.name;
    tractor.description = tractorModel.description;
    tractor.ownerPartyId = tractorModel.ownerPartyId;
    tractor.plateNo = tractorModel.plateNo;
    tractor.chassisNo = tractorModel.chassisNo;
    tractor.engineNo = tractorModel.engineNo;
    tractor.axleQuantity = tractorModel.axleQuantity;
    tractor.modelYearId = tractorModel.modelYearId;
    tractor.statuses = [];
    tractor.classifications = [];
    // Invoke update status
    tractor.updateStatus(tractorModel.statusTypeId);
    // Classify myself as a container
    tractor.updateClassification(AssetTypeCategoryId.Tractor);
    // Add to repository
    await this.assetRepository.add(tractor);

    return tractorModel;
  }

  async find(id: string): Promise<TractorModel> {
    const tractor = await this.assetRepository.find(id);

    const tractorModel = new TractorModel();
    tractorModel.id = tractor.id;
    tractorModel.name = tractor.name;
    tractorModel.description = tractor.description;
    tractorModel.ownerPartyId = tractor.ownerPartyId;
    tractorModel.statusTypeId = tractor.currentStatus.statusTypeId;
    tractorModel.plateNo = tractor.plateNo;
    tractorModel.chassisNo = tractor.chassisNo;
    tractorModel.engineNo = tractor.engineNo;
    tractorModel.axleQuantity = tractor.axleQuantity;
    tractorModel.modelYearId = tractor.modelYearId;

    return tractorModel;
  }

  async findAll(skip: number = 0, take: number = 20): Promise<TractorModel[]> {
    const tractors = await this.assetRepository.findAll(skip, take);

    return tractors.map(c => {
      const tractorModel = new TractorModel();
      tractorModel.id = c.id;
      tractorModel.name = c.name;
      tractorModel.description = c.description;
      tractorModel.ownerPartyId = c.ownerPartyId;
      tractorModel.statusTypeId = c.currentStatus.statusTypeId;
      tractorModel.plateNo = c.plateNo;
      tractorModel.chassisNo = c.chassisNo;
      tractorModel.engineNo = c.engineNo;
      tractorModel.axleQuantity = c.axleQuantity;
      tractorModel.modelYearId = c.modelYearId;

      return tractorModel;
    });
  }

  @Transactional()
  async delete(... tractorIds: string[]): Promise<void> {
    await this.assetRepository.remove(... tractorIds);
  }

  @Transactional()
  async update(tractorId: string, tractorModel: TractorModel): Promise<TractorModel> {
    const tractor = await this.assetRepository.find(tractorId);
    // Update
    tractor.name = tractorModel.name;
    tractor.description = tractorModel.description;
    tractor.ownerPartyId = tractorModel.ownerPartyId;
    tractor.plateNo = tractorModel.plateNo;
    tractor.chassisNo = tractorModel.chassisNo;
    tractor.engineNo = tractorModel.engineNo;
    tractor.axleQuantity = tractorModel.axleQuantity;
    tractor.modelYearId = tractorModel.modelYearId;
    // Change status
    tractor.updateStatus(tractorModel.statusTypeId);
    // Invoke the update on the repository
    await this.assetRepository.update(tractor);

    return tractorModel;
  }
}