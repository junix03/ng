import { Inject, Injectable } from '@nestjs/common';
import { ASSET_CONTAINER_REPO } from '@infrastructure/repository';
import { AssetRepository, AssetTypeCategoryId, Container } from '@domain/common';
import { IsString, IsUUID, Length } from 'class-validator';
import { Transactional } from '@infrastructure/repository/transactional';

/**
 * Data transfer object for this service.
 */
export class ContainerModel {
  @IsUUID()
  id: string;
  @Length(0, 53)
  name: string;
  @Length(0, 280)
  description: string;
  @IsUUID()
  ownerPartyId: string;
  @IsString()
  containerCode: string;
  @IsUUID()
  statusTypeId: string;
}

@Injectable()
export class ContainerService {
  constructor(
    @Inject(ASSET_CONTAINER_REPO)
    private readonly assetRepository: AssetRepository<Container>,
  ) {}

  @Transactional()
  async create(containerModel: ContainerModel): Promise<ContainerModel> {
    // Create the domain object
    const container = new Container();
    // Map the data transfer object
    container.id = containerModel.id;
    container.name = containerModel.name;
    container.description = containerModel.description;
    container.ownerPartyId = containerModel.ownerPartyId;
    container.containerCode = containerModel.containerCode;
    container.statuses = [];
    container.classifications = [];
    // Invoke update status
    container.updateStatus(containerModel.statusTypeId);
    // Classify myself as a container
    container.updateClassification(AssetTypeCategoryId.Container);
    // Add to repository
    await this.assetRepository.add(container);

    return containerModel;
  }

  async find(id: string): Promise<ContainerModel> {
    const container = await this.assetRepository.find(id);

    const containerModel = new ContainerModel();
    containerModel.id = container.id;
    containerModel.name = container.name;
    containerModel.description = container.description;
    containerModel.ownerPartyId = container.ownerPartyId;
    containerModel.containerCode = container.containerCode;
    containerModel.statusTypeId = container.currentStatus.statusTypeId;

    return containerModel;
  }

  async findAll(skip: number = 0, take: number = 20): Promise<ContainerModel[]> {
    const containers = await this.assetRepository.findAll(skip, take);

    return containers.map(c => {
      const containerModel = new ContainerModel();
      containerModel.id = c.id;
      containerModel.name = c.name;
      containerModel.description = c.description;
      containerModel.ownerPartyId = c.ownerPartyId;
      containerModel.containerCode = c.containerCode;
      containerModel.statusTypeId = c.currentStatus.statusTypeId;
      return containerModel;
    });
  }

  @Transactional()
  async delete(... containerIds: string[]): Promise<void> {
    await this.assetRepository.remove(... containerIds);
  }

  @Transactional()
  async update(containerId: string, containerModel: ContainerModel): Promise<ContainerModel> {
    const container = await this.assetRepository.find(containerId);
    // Update
    container.name = containerModel.name;
    container.description = containerModel.description;
    container.ownerPartyId = containerModel.ownerPartyId;
    container.containerCode = containerModel.containerCode;
    // Change status
    container.updateStatus(containerModel.statusTypeId);
    // Invoke the update on the repository
    await this.assetRepository.update(container);

    return containerModel;
  }
}