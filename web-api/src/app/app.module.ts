import { Module } from '@nestjs/common';
import { InfrastructureModule } from '@infrastructure/infrastructure.module';
import { AuthModule } from '@crosscutting/auth';
import { assetServices, partyServices } from '@app/common';

/**
 * Module that represents the application layer.
 * Imports the infrastructure layer since it is a direct dependency.
 *
 * Any other modules imported here are may be considered part of cross
 * cutting concerns.
 */
@Module({
  imports: [
    InfrastructureModule,
    AuthModule,
  ],
  providers: [
    ... partyServices,
    ... assetServices,
  ],
  exports: [
    ... partyServices,
    ... assetServices,
  ],
})
export class AppModule {}
