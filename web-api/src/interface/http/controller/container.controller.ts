import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ContainerModel, ContainerService } from '@app/common';

@Controller('api/common-object/asset/container')
export class ContainerController {
  constructor(
    private readonly containerService: ContainerService,
  ) {}

  @Post()
  async create(@Body() containerModel: ContainerModel): Promise<ContainerModel> {
    return await this.containerService.create(containerModel);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<ContainerModel> {
    return await this.containerService.find(id);
  }

  @Get()
  async findAll(@Query('skip') skip: number, @Query('take') take: number): Promise<ContainerModel[]> {
    if (skip !== undefined && take !== undefined) {
      return await this.containerService.findAll(skip, take);
    }
    return await this.containerService.findAll();
  }

  @Put(':id')
  async update(@Param('id') containerId: string, @Body() containerModel: ContainerModel): Promise<ContainerModel> {
    return await this.containerService.update(containerId, containerModel);
  }

  @Delete(':id')
  async delete(@Param('id') containerId: string) {
    return await this.containerService.delete(containerId);
  }

  @Post('delete')
  async deleteMultiple(@Body() containerIds: string[]) {
    return await this.containerService.delete(... containerIds);
  }
}