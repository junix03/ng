import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { TractorModel, TractorService } from '@app/common';

@Controller('api/common-object/asset/tractor')
export class TractorController {
  constructor(
    private readonly tractorService: TractorService,
  ) {}

  @Post()
  async create(@Body() tractorModel: TractorModel): Promise<TractorModel> {
    return await this.tractorService.create(tractorModel);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<TractorModel> {
    return await this.tractorService.find(id);
  }

  @Get()
  async findAll(@Query('skip') skip: number, @Query('take') take: number): Promise<TractorModel[]> {
    if (skip !== undefined && take !== undefined) {
      return await this.tractorService.findAll(skip, take);
    }
    return await this.tractorService.findAll();
  }

  @Put(':id')
  async update(@Param('id') tractorId: string, @Body() tractorModel: TractorModel): Promise<TractorModel> {
    return await this.tractorService.update(tractorId, tractorModel);
  }

  @Delete(':id')
  async delete(@Param('id') tractorId: string) {
    return await this.tractorService.delete(tractorId);
  }

  @Post('delete')
  async deleteMultiple(@Body() tractorIds: string[]) {
    return await this.tractorService.delete(... tractorIds);
  }
}