import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ChassisModel, ChassisService } from '@app/common/asset';

@Controller('api/common-object/asset/chassis')
export class ChassisController {
  constructor(
    private readonly chassisService: ChassisService,
  ) {}

  @Post()
  async create(@Body() chassisModel: ChassisModel): Promise<ChassisModel> {
    return await this.chassisService.create(chassisModel);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<ChassisModel> {
    return await this.chassisService.find(id);
  }

  @Get()
  async findAll(@Query('skip') skip: number, @Query('take') take: number): Promise<ChassisModel[]> {
    if (skip !== undefined && take !== undefined) {
      return await this.chassisService.findAll(skip, take);
    }
    return await this.chassisService.findAll();
  }

  @Put(':id')
  async update(@Param('id') chassisId: string, @Body() chassisModel: ChassisModel): Promise<ChassisModel> {
    return await this.chassisService.update(chassisId, chassisModel);
  }

  @Delete(':id')
  async delete(@Param('id') chassisId: string) {
    return await this.chassisService.delete(chassisId);
  }

  @Post('delete')
  async deleteMultiple(@Body() chassisIds: string[]) {
    return await this.chassisService.delete(... chassisIds);
  }
}