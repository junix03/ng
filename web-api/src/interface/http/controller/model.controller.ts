import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ModelModel, ModelService } from '@app/common';

@Controller('api/common-object/asset/model')
export class ModelController {
  constructor(
    private readonly modelService: ModelService,
  ) {}

  @Post()
  async create(@Body() modelModel: ModelModel): Promise<ModelModel> {
    return await this.modelService.create(modelModel);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<ModelModel> {
    return await this.modelService.find(id);
  }

  @Get()
  async findAll(@Query('skip') skip: number, @Query('take') take: number): Promise<ModelModel[]> {
    if (skip !== undefined && take !== undefined) {
      return await this.modelService.findAll(skip, take);
    }
    return await this.modelService.findAll();
  }

  @Put(':id')
  async update(@Param('id') modelId: string, @Body() modelModel: ModelModel): Promise<ModelModel> {
    return await this.modelService.update(modelId, modelModel);
  }

  @Delete(':id')
  async delete(@Param('id') modelId: string) {
    return await this.modelService.delete(modelId);
  }

  @Post('delete')
  async deleteMultiple(@Body() modelIds: string[]) {
    return await this.modelService.delete(... modelIds);
  }
}