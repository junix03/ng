import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ModelYearModel, ModelYearService } from '@app/common';

@Controller('api/common-object/asset/modelyear')
export class ModelYearController {
  constructor(
    private readonly modelYearService: ModelYearService,
  ) {}

  @Post()
  async create(@Body() modelYearModel: ModelYearModel): Promise<ModelYearModel> {
    return await this.modelYearService.create(modelYearModel);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<ModelYearModel> {
    return await this.modelYearService.find(id);
  }

  @Get()
  async findAll(@Query('skip') skip: number, @Query('take') take: number): Promise<ModelYearModel[]> {
    if (skip !== undefined && take !== undefined) {
      return await this.modelYearService.findAll(skip, take);
    }
    return await this.modelYearService.findAll();
  }

  @Put(':id')
  async update(@Param('id') modelYearId: string, @Body() modelYearModel: ModelYearModel): Promise<ModelYearModel> {
    return await this.modelYearService.update(modelYearId, modelYearModel);
  }

  @Delete(':id')
  async delete(@Param('id') modelYearId: string) {
    return await this.modelYearService.delete(modelYearId);
  }

  @Post('delete')
  async deleteMultiple(@Body() modelYearIds: string[]) {
    return await this.modelYearService.delete(... modelYearIds);
  }
}