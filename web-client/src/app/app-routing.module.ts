import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from '@auth/containers';
import { LayoutComponent } from '@shell/containers';
import { SessionGuard } from '@auth/guards';
import { NoSessionGuard } from '@auth/guards';
import { NavigationListComponent } from '@shell/containers';

const routes: Routes = [
  {
    path: 'login',
    canActivate: [NoSessionGuard],
    component: LoginPageComponent
  },
  {
    path: '',
    canActivate: [SessionGuard],
    component: LayoutComponent,
    children: [
      {
        path: 'common-object/asset',
        loadChildren: '@modules/asset/asset.module#AssetModule'
      },
      {
        path: 'common-object/party',
        loadChildren: '@modules/party/party.module#PartyModule'
      },
      {
        path: '**',
        pathMatch: 'full',
        component: NavigationListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
