import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionInterceptor } from '@auth/interceptors';
import { AuthModule } from '@auth/auth.module';
import { environment } from '@environments/environment';
import { ShellModule } from '@shell/shell.module';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoadingBarHttpClientModule,
    // This enables Redux DevTools support for Akita
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    // Router store
    AkitaNgRouterStoreModule.forRoot(),
    // Authentication and Shell should be loaded at initial startup
    AuthModule,
    ShellModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SessionInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
