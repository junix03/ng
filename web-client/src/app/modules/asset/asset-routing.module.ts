import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationListComponent } from '@shell/containers';
import {
          ContainerEditPageComponent, ContainerListPageComponent, ContainerNewPageComponent,
          TractorEditPageComponent, TractorListPageComponent, TractorNewPageComponent,
          ChassisEditPageComponent, ChassisListPageComponent, ChassisNewPageComponent,
          ModelEditPageComponent, ModelListPageComponent, ModelNewPageComponent,
          ModelYearEditPageComponent, ModelYearListPageComponent, ModelYearNewPageComponent
        } from '@modules/asset/containers';
import { ContainerResolver, TractorResolver, ChassisResolver, ModelResolver, ModelYearResolver } from '@modules/asset/services';

const routes: Routes = [
  {
    path: '',
    component: NavigationListComponent
  },
  {
    path: 'container',
    component: ContainerListPageComponent
  },
  {
    path: 'container/new',
    component: ContainerNewPageComponent
  },
  {
    path: 'container/:id',
    component: ContainerEditPageComponent,
    resolve: {
      container: ContainerResolver
    }
  },
  {
    path: 'tractor',
    component: TractorListPageComponent
  },
  {
    path: 'tractor/new',
    component: TractorNewPageComponent
  },
  {
    path: 'tractor/:id',
    component: TractorEditPageComponent,
    resolve: {
      container: TractorResolver
    }
  },
  {
    path: 'chassis',
    component: ChassisListPageComponent
  },
  {
    path: 'chassis/new',
    component: ChassisNewPageComponent
  },
  {
    path: 'chassis/:id',
    component: ChassisEditPageComponent,
    resolve: {
      container: ChassisResolver
    }
  },
  {
    path: 'model',
    component: ModelListPageComponent
  },
  {
    path: 'model/new',
    component: ModelNewPageComponent
  },
  {
    path: 'model/:id',
    component: ModelEditPageComponent,
    resolve: {
      container: ModelResolver
    }
  },
  {
    path: 'modelyear',
    component: ModelYearListPageComponent
  },
  {
    path: 'modelyear/new',
    component: ModelYearNewPageComponent
  },
  {
    path: 'modelyear/:id',
    component: ModelYearEditPageComponent,
    resolve: {
      container: ModelYearResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetRoutingModule {}
