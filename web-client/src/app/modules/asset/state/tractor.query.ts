import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { TractorState, TractorStore } from './tractor.store';
import { Tractor } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class TractorQuery extends QueryEntity<TractorState, Tractor> {

  constructor(protected store: TractorStore) {
    super(store);
  }

}
