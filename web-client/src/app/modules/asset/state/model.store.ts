import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Model } from '@modules/asset/models';

export interface ModelState extends EntityState<Model> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'model' })
export class ModelStore extends EntityStore<ModelState, Model> {

  constructor() {
    super({ loading: false });
  }

}
