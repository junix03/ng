import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Container } from '@modules/asset/models';

export interface ContainerState extends EntityState<Container> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'container' })
export class ContainerStore extends EntityStore<ContainerState, Container> {

  constructor() {
    super({ loading: false });
  }

}

