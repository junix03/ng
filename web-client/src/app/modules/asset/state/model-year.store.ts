import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { ModelYear } from '@modules/asset/models';

export interface ModelYearState extends EntityState<ModelYear> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'modelYear' })
export class ModelYearStore extends EntityStore<ModelYearState, ModelYear> {

  constructor() {
    super({ loading: false });
  }

}
