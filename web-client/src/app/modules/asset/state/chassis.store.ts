import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Chassis } from '@modules/asset/models';

export interface ChassisState extends EntityState<Chassis> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'chassis' })
export class ChassisStore extends EntityStore<ChassisState, Chassis> {

  constructor() {
    super({ loading: false });
  }

}
