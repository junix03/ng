import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Tractor } from '@modules/asset/models';

export interface TractorState extends EntityState<Tractor> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'tractor' })
export class TractorStore extends EntityStore<TractorState, Tractor> {

  constructor() {
    super({ loading: false });
  }

}
