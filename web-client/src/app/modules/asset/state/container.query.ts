import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ContainerState, ContainerStore } from './container.store';
import { Container } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ContainerQuery extends QueryEntity<ContainerState, Container> {

  constructor(protected store: ContainerStore) {
    super(store);
  }

}
