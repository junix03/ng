import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ModelState, ModelStore } from './model.store';
import { Model } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ModelQuery extends QueryEntity<ModelState, Model> {

  constructor(protected store: ModelStore) {
    super(store);
  }

}
