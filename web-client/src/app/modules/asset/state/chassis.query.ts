import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ChassisState, ChassisStore } from './chassis.store';
import { Chassis } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ChassisQuery extends QueryEntity<ChassisState, Chassis> {

  constructor(protected store: ChassisStore) {
    super(store);
  }

}
