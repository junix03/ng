import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ModelYearState, ModelYearStore } from './model-year.store';
import { ModelYear } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ModelYearQuery extends QueryEntity<ModelYearState, ModelYear> {

  constructor(protected store: ModelYearStore) {
    super(store);
  }

}
