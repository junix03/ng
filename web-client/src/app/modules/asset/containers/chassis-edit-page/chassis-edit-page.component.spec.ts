import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisEditPageComponent } from './chassis-edit-page.component';

describe('ChassisEditPageComponent', () => {
  let component: ChassisEditPageComponent;
  let fixture: ComponentFixture<ChassisEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
