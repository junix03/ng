import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FormQuery, FormStore } from '@shell/state';
import { TractorQuery } from '@modules/asset/state';
import { TractorService } from '@modules/asset/services';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tractor-edit-page',
  templateUrl: './tractor-edit-page.component.html',
  styleUrls: ['./tractor-edit-page.component.sass']
})
export class TractorEditPageComponent implements OnInit, OnDestroy {

  // Check query params for edit flag
  edit$ = this.route.queryParams.pipe(
    map(params => params.edit ? params.edit : 'false')
  );
  editFlag = false;
  formId = 'tractorMaster';
  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;
  tractorId = this.route.snapshot.paramMap.get('id');

  constructor(
    private readonly formQuery: FormQuery,
    private readonly formStore: FormStore,
    private readonly tractorService: TractorService,
    private readonly tractorQuery: TractorQuery,
    private readonly toolbarService: ToolbarService,
    private readonly route: ActivatedRoute
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.tractorQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
    // Take only once, so stream will end immediately.
    this.edit$.pipe(take(1))
      .subscribe(edit => {
        if (edit === 'false') {
          this.disableEdit();
          this.editFlag = false;
        } else {
          this.editFlag = true;
        }
      });
    // Fill forms with defaults
    this.formStore.updateForm(this.formId, {
      value: this.tractorQuery.getSnapshot().entities[this.tractorId]
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of master form
        const { value } = this.formQuery.getSnapshot().entities[this.formId];
        // Get existing uuid
        const tractor = { ... value, id: this.tractorId };
        this.tractorService.update(this.tractorId, tractor);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

  /**
   * Changes edit mode.
   * @param value value emitted from switch component
   */
  onEditModeChange(value) {
    if (value) {
      // Enable master form (or various forms, just add them here)
      this.formStore.setFormEnabled(this.formId);
    } else {
      // Disable master form (and etc)
      this.disableEdit();
    }
    this.editFlag = value;
  }

  /**
   * Disables edit mode.
   */
  disableEdit() {
    this.formStore.setFormDisabled(this.formId);
  }

}
