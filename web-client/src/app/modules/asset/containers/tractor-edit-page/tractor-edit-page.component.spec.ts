import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorEditPageComponent } from './tractor-edit-page.component';

describe('TractorEditPageComponent', () => {
  let component: TractorEditPageComponent;
  let fixture: ComponentFixture<TractorEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
