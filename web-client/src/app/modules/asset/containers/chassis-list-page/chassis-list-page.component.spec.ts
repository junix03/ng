import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisListPageComponent } from './chassis-list-page.component';

describe('ChassisListPageComponent', () => {
  let component: ChassisListPageComponent;
  let fixture: ComponentFixture<ChassisListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
