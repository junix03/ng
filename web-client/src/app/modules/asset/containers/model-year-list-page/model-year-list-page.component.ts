import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { listToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { ModelYear } from '@modules/asset/models';
import { ModelYearQuery } from '@modules/asset/state';
import { ModelYearService } from '@modules/asset/services';


@Component({
  selector: 'app-model-year-list-page',
  templateUrl: './model-year-list-page.component.html',
  styleUrls: ['./model-year-list-page.component.sass']
})
export class ModelYearListPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  selectedItems: ModelYear[];

  skip = 0;

  modelYears$ = this.modelYearQuery.selectAll();

  constructor(
    readonly modelYearService: ModelYearService,
    private readonly modelYearQuery: ModelYearQuery,
    private readonly toolbarService: ToolbarService,
    private readonly modal: NzModalService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    // Load a toolbar preset
    this.toolbarService.loadToolbar(listToolbars());
    // Subscribe to toolbar clicks
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    // Fetch all
    this.modelYearService.fetchBatch();
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
  }

  onBottomScrolled() {
    this.skip += 20;
    // Fetch all
    this.modelYearService.fetchBatch(this.skip);
  }

  onRowSelected(modelYears: ModelYear[]) {
    this.selectedItems = modelYears;
    if (this.selectedItems.length > 0) {
      this.toolbarService.enableToolbar(ToolbarType.Delete);
    } else {
      this.toolbarService.disableToolbar(ToolbarType.Delete);
    }
    if (this.selectedItems.length === 1) {
      this.toolbarService.enableToolbar(ToolbarType.View, ToolbarType.Edit);
    } else {
      this.toolbarService.disableToolbar(ToolbarType.View, ToolbarType.Edit);
    }
  }

  get selectedItemId() {
    return this.selectedItems[0] ? this.selectedItems[0].id : '';
  }

  onRowDoubleClicked(modelYear: ModelYear) {
    this.router.navigate([`${this.router.url}/${modelYear.id}`]);
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.View: {
        this.router.navigate([`${this.router.url}/${this.selectedItemId}`]);
        break;
      }
      case ToolbarType.New: {
        this.router.navigate([`${this.router.url}/new`]);
        break;
      }
      case ToolbarType.Edit: {
        this.router.navigate([`${this.router.url}/${this.selectedItemId}`], {
          queryParams: {
            edit: true
          }
        });
        break;
      }
      case ToolbarType.Delete: {
        this.confirmDelete();
        break;
      }
    }
  }

  confirmDelete() {
    this.modal.confirm({
      nzTitle: this.deleteModalTitle,
      nzContent: this.deleteModalContent,
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => this.attemptDelete(),
      nzCancelText: 'No'
    });
  }

  get deleteModalTitle() {
    if (this.selectedItems.length === 1) {
      return 'Delete Item';
    } else {
      return 'Delete Multiple Items';
    }
  }

  get deleteModalContent() {
    const size = this.selectedItems.length;
    if (size === 1) {
      return 'Are you sure you want to delete this item?';
    } else {
      return `Are you sure you want to delete these ${size} items?`;
    }
  }

  attemptDelete() {
    // Attempt to delete selected items
    const selectedIds = this.selectedItems.map(p => p.id);
    this.modelYearService.delete(... selectedIds);
  }

}
