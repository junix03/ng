import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelYearListPageComponent } from './model-year-list-page.component';

describe('ModelYearListPageComponent', () => {
  let component: ModelYearListPageComponent;
  let fixture: ComponentFixture<ModelYearListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelYearListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelYearListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
