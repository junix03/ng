import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorNewPageComponent } from './tractor-new-page.component';

describe('TractorNewPageComponent', () => {
  let component: TractorNewPageComponent;
  let fixture: ComponentFixture<TractorNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
