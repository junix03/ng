import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormQuery } from '@shell/state';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { TractorQuery } from '@modules/asset/state';
import { TractorService } from '@modules/asset/services';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-tractor-new-page',
  templateUrl: './tractor-new-page.component.html',
  styleUrls: ['./tractor-new-page.component.sass']
})
export class TractorNewPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;

  constructor(
    private readonly formQuery: FormQuery,
    private readonly tractorService: TractorService,
    private readonly tractorQuery: TractorQuery,
    private readonly toolbarService: ToolbarService
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.tractorQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of tractor master form
        const { value } = this.formQuery.getSnapshot().entities['tractorMaster'];
        // Generate uuid
        const tractor = { ... value, id: uuid() };
        this.tractorService.create(tractor);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

}
