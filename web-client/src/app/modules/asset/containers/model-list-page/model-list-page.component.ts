import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { listToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { Model } from '@modules/asset/models';
import { ModelQuery } from '@modules/asset/state';
import { ModelService } from '@modules/asset/services';

@Component({
  selector: 'app-model-list-page',
  templateUrl: './model-list-page.component.html',
  styleUrls: ['./model-list-page.component.sass']
})
export class ModelListPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  selectedItems: Model[];

  skip = 0;

  models$ = this.modelQuery.selectAll();

  constructor(
    readonly modelService: ModelService,
    private readonly modelQuery: ModelQuery,
    private readonly toolbarService: ToolbarService,
    private readonly modal: NzModalService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    // Load a toolbar preset
    this.toolbarService.loadToolbar(listToolbars());
    // Subscribe to toolbar clicks
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    // Fetch all
    this.modelService.fetchBatch();
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
  }

  onBottomScrolled() {
    this.skip += 20;
    // Fetch all
    this.modelService.fetchBatch(this.skip);
  }

  onRowSelected(models: Model[]) {
    this.selectedItems = models;
    if (this.selectedItems.length > 0) {
      this.toolbarService.enableToolbar(ToolbarType.Delete);
    } else {
      this.toolbarService.disableToolbar(ToolbarType.Delete);
    }
    if (this.selectedItems.length === 1) {
      this.toolbarService.enableToolbar(ToolbarType.View, ToolbarType.Edit);
    } else {
      this.toolbarService.disableToolbar(ToolbarType.View, ToolbarType.Edit);
    }
  }

  get selectedItemId() {
    return this.selectedItems[0] ? this.selectedItems[0].id : '';
  }

  onRowDoubleClicked(model: Model) {
    this.router.navigate([`${this.router.url}/${model.id}`]);
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.View: {
        this.router.navigate([`${this.router.url}/${this.selectedItemId}`]);
        break;
      }
      case ToolbarType.New: {
        this.router.navigate([`${this.router.url}/new`]);
        break;
      }
      case ToolbarType.Edit: {
        this.router.navigate([`${this.router.url}/${this.selectedItemId}`], {
          queryParams: {
            edit: true
          }
        });
        break;
      }
      case ToolbarType.Delete: {
        this.confirmDelete();
        break;
      }
    }
  }

  confirmDelete() {
    this.modal.confirm({
      nzTitle: this.deleteModalTitle,
      nzContent: this.deleteModalContent,
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => this.attemptDelete(),
      nzCancelText: 'No'
    });
  }

  get deleteModalTitle() {
    if (this.selectedItems.length === 1) {
      return 'Delete Item';
    } else {
      return 'Delete Multiple Items';
    }
  }

  get deleteModalContent() {
    const size = this.selectedItems.length;
    if (size === 1) {
      return 'Are you sure you want to delete this item?';
    } else {
      return `Are you sure you want to delete these ${size} items?`;
    }
  }

  attemptDelete() {
    // Attempt to delete selected items
    const selectedIds = this.selectedItems.map(p => p.id);
    this.modelService.delete(... selectedIds);
  }

}
