import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelListPageComponent } from './model-list-page.component';

describe('ModelListPageComponent', () => {
  let component: ModelListPageComponent;
  let fixture: ComponentFixture<ModelListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
