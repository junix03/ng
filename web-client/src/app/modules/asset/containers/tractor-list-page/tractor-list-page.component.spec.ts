import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorListPageComponent } from './tractor-list-page.component';

describe('TractorListPageComponent', () => {
  let component: TractorListPageComponent;
  let fixture: ComponentFixture<TractorListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
