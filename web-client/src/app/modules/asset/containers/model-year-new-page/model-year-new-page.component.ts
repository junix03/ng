import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormQuery } from '@shell/state';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ModelYearQuery } from '@modules/asset/state';
import { ModelYearService } from '@modules/asset/services';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-model-year-new-page',
  templateUrl: './model-year-new-page.component.html',
  styleUrls: ['./model-year-new-page.component.sass']
})
export class ModelYearNewPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;

  constructor(
    private readonly formQuery: FormQuery,
    private readonly modelYearService: ModelYearService,
    private readonly modelYearQuery: ModelYearQuery,
    private readonly toolbarService: ToolbarService
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.modelYearQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of person master form
        const { value } = this.formQuery.getSnapshot().entities['modelYearMaster'];
        // Generate uuid
        const modelYear = { ... value, id: uuid() };
        this.modelYearService.create(modelYear);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }


}
