import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelYearNewPageComponent } from './model-year-new-page.component';

describe('ModelYearNewPageComponent', () => {
  let component: ModelYearNewPageComponent;
  let fixture: ComponentFixture<ModelYearNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelYearNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelYearNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
