import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisNewPageComponent } from './chassis-new-page.component';

describe('ChassisNewPageComponent', () => {
  let component: ChassisNewPageComponent;
  let fixture: ComponentFixture<ChassisNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
