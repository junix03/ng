import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormQuery } from '@shell/state';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ChassisQuery } from '@modules/asset/state';
import { ChassisService } from '@modules/asset/services';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-chassis-new-page',
  templateUrl: './chassis-new-page.component.html',
  styleUrls: ['./chassis-new-page.component.sass']
})
export class ChassisNewPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;

  constructor(
    private readonly formQuery: FormQuery,
    private readonly chassisService: ChassisService,
    private readonly chassisQuery: ChassisQuery,
    private readonly toolbarService: ToolbarService
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.chassisQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of person master form
        const { value } = this.formQuery.getSnapshot().entities['chassisMaster'];
        // Generate uuid
        const chassis = { ... value, id: uuid() };
        this.chassisService.create(chassis);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

}
