import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerNewPageComponent } from './container-new-page.component';

describe('ContainerNewPageComponent', () => {
  let component: ContainerNewPageComponent;
  let fixture: ComponentFixture<ContainerNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
