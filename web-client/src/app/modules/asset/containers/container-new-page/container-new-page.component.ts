import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormQuery } from '@shell/state';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ContainerQuery } from '@modules/asset/state';
import { ContainerService } from '@modules/asset/services';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-container-new-page',
  templateUrl: './container-new-page.component.html',
  styleUrls: ['./container-new-page.component.sass']
})
export class ContainerNewPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;

  constructor(
    private readonly formQuery: FormQuery,
    private readonly containerService: ContainerService,
    private readonly containerQuery: ContainerQuery,
    private readonly toolbarService: ToolbarService
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.containerQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of person master form
        const { value } = this.formQuery.getSnapshot().entities['containerMaster'];
        // Generate uuid
        const container = { ... value, id: uuid() };
        this.containerService.create(container);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

}
