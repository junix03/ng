import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelNewPageComponent } from './model-new-page.component';

describe('ModelNewPageComponent', () => {
  let component: ModelNewPageComponent;
  let fixture: ComponentFixture<ModelNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
