import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormQuery } from '@shell/state';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ModelQuery } from '@modules/asset/state';
import { ModelService } from '@modules/asset/services';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-model-new-page',
  templateUrl: './model-new-page.component.html',
  styleUrls: ['./model-new-page.component.sass']
})
export class ModelNewPageComponent implements OnInit, OnDestroy {

  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;

  constructor(
    private readonly formQuery: FormQuery,
    private readonly modelService: ModelService,
    private readonly modelQuery: ModelQuery,
    private readonly toolbarService: ToolbarService
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.modelQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of person master form
        const { value } = this.formQuery.getSnapshot().entities['modelMaster'];
        // Generate uuid
        const model = { ... value, id: uuid() };
        this.modelService.create(model);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

}
