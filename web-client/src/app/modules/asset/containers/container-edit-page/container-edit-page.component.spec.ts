import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerEditPageComponent } from './container-edit-page.component';

describe('ContainerEditPageComponent', () => {
  let component: ContainerEditPageComponent;
  let fixture: ComponentFixture<ContainerEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
