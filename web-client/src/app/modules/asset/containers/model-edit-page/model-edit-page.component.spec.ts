import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelEditPageComponent } from './model-edit-page.component';

describe('ModelEditPageComponent', () => {
  let component: ModelEditPageComponent;
  let fixture: ComponentFixture<ModelEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
