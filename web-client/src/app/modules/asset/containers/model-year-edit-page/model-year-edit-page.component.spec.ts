import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelYearEditPageComponent } from './model-year-edit-page.component';

describe('ModelYearEditPageComponent', () => {
  let component: ModelYearEditPageComponent;
  let fixture: ComponentFixture<ModelYearEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelYearEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelYearEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
