import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FormQuery, FormStore } from '@shell/state';
import { ModelYearQuery } from '@modules/asset/state';
import { ModelYearService } from '@modules/asset/services';
import { formToolbars, ToolbarService, ToolbarType, ToolbarTypes } from '@shell/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-model-year-edit-page',
  templateUrl: './model-year-edit-page.component.html',
  styleUrls: ['./model-year-edit-page.component.sass']
})
export class ModelYearEditPageComponent implements OnInit, OnDestroy {

  // Check query params for edit flag
  edit$ = this.route.queryParams.pipe(
    map(params => params.edit ? params.edit : 'false')
  );
  editFlag = false;
  formId = 'modelYearMaster';
  toolbarClickSubscription: Subscription;
  newToolbarLoadingSubscription: Subscription;
  modelYearId = this.route.snapshot.paramMap.get('id');

  constructor(
    private readonly formQuery: FormQuery,
    private readonly formStore: FormStore,
    private readonly modelYearService: ModelYearService,
    private readonly modelYearQuery: ModelYearQuery,
    private readonly toolbarService: ToolbarService,
    private readonly route: ActivatedRoute
  ) { }

  ngOnInit() {
    // Preset toolbar, please change when needed
    this.toolbarService.loadToolbar(formToolbars());
    this.toolbarClickSubscription = this.toolbarService.toolbarClicked$
      .subscribe(type => this.onToolbarItemClicked(type));
    this.newToolbarLoadingSubscription = this.modelYearQuery.selectLoading().subscribe(loading => {
      this.toolbarService.updateToolbarLoading(ToolbarType.Save, loading);
    });
    // Take only once, so stream will end immediately.
    this.edit$.pipe(take(1))
      .subscribe(edit => {
        if (edit === 'false') {
          this.disableEdit();
          this.editFlag = false;
        } else {
          this.editFlag = true;
        }
      });
    // Fill forms with defaults
    this.formStore.updateForm(this.formId, {
      value: this.modelYearQuery.getSnapshot().entities[this.modelYearId]
    });
  }

  onToolbarItemClicked(type: ToolbarTypes) {
    switch (type) {
      case ToolbarType.Save: {
        // Get a snapshot of master form
        const { value } = this.formQuery.getSnapshot().entities[this.formId];
        // Get existing uuid
        const modelYear = { ... value, id: this.modelYearId };
        this.modelYearService.update(this.modelYearId, modelYear);
        break;
      }
    }
  }

  ngOnDestroy() {
    this.toolbarClickSubscription.unsubscribe();
    this.newToolbarLoadingSubscription.unsubscribe();
  }

  /**
   * Changes edit mode.
   * @param value value emitted from switch component
   */
  onEditModeChange(value) {
    if (value) {
      // Enable master form (or various forms, just add them here)
      this.formStore.setFormEnabled(this.formId);
    } else {
      // Disable master form (and etc)
      this.disableEdit();
    }
    this.editFlag = value;
  }

  /**
   * Disables edit mode.
   */
  disableEdit() {
    this.formStore.setFormDisabled(this.formId);
  }


}
