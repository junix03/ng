import { NgModule } from '@angular/core';
import { AssetRoutingModule } from './asset-routing.module';
import { ContainerEditPageComponent, ContainerListPageComponent, ContainerNewPageComponent, } from './containers';
import { ShellModule } from '@shell/shell.module';
import { TractorFormComponent,  TractorListComponent,
         ChassisFormComponent, ChassisListComponent,
         ContainerFormComponent, ContainerListComponent,
         ModelYearFormComponent, ModelYearListComponent,
         ModelFormComponent, ModelListComponent  } from './components';
import { TractorEditPageComponent, TractorListPageComponent, TractorNewPageComponent,
         ChassisEditPageComponent, ChassisListPageComponent, ChassisNewPageComponent,
         ModelEditPageComponent, ModelListPageComponent, ModelNewPageComponent,
         ModelYearEditPageComponent, ModelYearListPageComponent, ModelYearNewPageComponent  } from './containers';

@NgModule({
  imports: [
    ShellModule,
    AssetRoutingModule
  ],
  declarations: [
    ContainerFormComponent,
    ContainerListComponent,
    ContainerListPageComponent,
    ContainerNewPageComponent,
    ContainerEditPageComponent,
    TractorFormComponent,
    TractorListComponent,
    TractorEditPageComponent,
    TractorListPageComponent,
    TractorNewPageComponent,
    ChassisFormComponent,
    ChassisListComponent,
    ModelYearFormComponent,
    ModelFormComponent,
    ModelListComponent,
    ModelYearListComponent,
    ChassisEditPageComponent,
    ChassisListPageComponent,
    ChassisNewPageComponent,
    ModelNewPageComponent,
    ModelEditPageComponent,
    ModelListPageComponent,
    ModelYearListPageComponent,
    ModelYearNewPageComponent,
    ModelYearEditPageComponent,
  ]
})
export class AssetModule {}
