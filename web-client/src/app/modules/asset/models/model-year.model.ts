export interface ModelYear {
  id: string;
  name: string;
  description: string;
  modelId: string;
}
