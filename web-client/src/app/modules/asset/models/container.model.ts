export interface Container {
  id: string;
  name: string;
  description: string;
  ownerPartyId: string;
  containerCode: string;
  statusTypeId: string;
}
