export interface Tractor {
  id: string;
  name: string;
  description: string;
  ownerPartyId: string;
  plateNo: string;
  chassisNo: string;
  engineNo: string;
  axleQuantity: number;
  modelYearId: string;
  statusTypeId: string;
}
