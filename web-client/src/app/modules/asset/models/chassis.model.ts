export interface Chassis {
  id: string;
  name: string;
  description: string;
  ownerPartyId: string;
  plateNo: string;
  chassisNo: string;
  axleQuantity: number;
  statusTypeId: string;
}
