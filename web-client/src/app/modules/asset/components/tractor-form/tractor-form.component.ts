import { Component, OnInit } from '@angular/core';
import { FormFields } from '@shell/models';
import { OrganizationService } from '@modules/party/services';
import { ModelYearService } from '@modules/asset/services';
import { OrganizationQuery } from '@modules/party/state';
import { ModelYearQuery } from '@modules/asset/state';


@Component({
  selector: 'app-tractor-form',
  templateUrl: './tractor-form.component.html',
  styleUrls: ['./tractor-form.component.sass']
})
export class TractorFormComponent implements OnInit {

  fields: FormFields;

  constructor(
    private readonly organizationService: OrganizationService,
    private readonly organizationQuery: OrganizationQuery,
    private readonly modelYearService: ModelYearService,
    private readonly modelYearQuery: ModelYearQuery,
  ) { }

  ngOnInit() {
    this.organizationService.fetchAll();
    this.modelYearService.fetchAll();
    this.fields = [
      {
        name: 'name',
        type: 'input',
        inputType: 'text',
        label: 'Display Name',
      },
      {
        name: 'description',
        type: 'textarea',
        label: 'Description',
      },
      {
        name: 'ownerPartyId',
        type: 'browse',
        label: 'Owner',
        displayedValueFieldName: 'ownerName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.organizationQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        },
      },
      {
        name: 'plateNo',
        type: 'input',
        inputType: 'text',
        label: 'Plate No',
      },
      {
        name: 'chassisNo',
        type: 'input',
        inputType: 'text',
        label: 'Chassis No'
      },
      {
        name: 'engineNo',
        type: 'input',
        inputType: 'text',
        label: 'Engine No',
      },
      {
        name: 'axleQuantity',
        type: 'input',
        inputType: 'number',
        label: 'Axle Qty'
      },
      {
        name: 'modelYearId',
        type: 'browse',
        label: 'Model Year',
        displayedValueFieldName: 'modelYearName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.modelYearQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        },
      },
      {
        name: 'statusTypeId',
        type: 'select',
        label: 'Status',
        options: [
          {
            name: 'Draft',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65b0'
          },
          {
            name: 'Available',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e0'
          },
          {
            name: 'Assigned',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e1'
          },
          {
            name: 'Running',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e2',
          },
          {
            name: 'Idle',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e3'
          },
          {
            name: 'Under Repair',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e4'
          },
          {
            name: 'Retired',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e5'
          },
          {
            name: 'Disposed',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e6'
          }
        ]
      }
    ];
  }

}
