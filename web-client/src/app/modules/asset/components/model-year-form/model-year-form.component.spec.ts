import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelYearFormComponent } from './model-year-form.component';

describe('ModelYearFormComponent', () => {
  let component: ModelYearFormComponent;
  let fixture: ComponentFixture<ModelYearFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelYearFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelYearFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
