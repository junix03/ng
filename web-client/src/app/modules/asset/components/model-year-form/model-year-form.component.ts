import { Component, OnInit } from '@angular/core';
import { FormFields } from '@shell/models';
import { ModelService } from '@modules/asset/services';
import { ModelQuery } from '@modules/asset/state';

@Component({
  selector: 'app-model-year-form',
  templateUrl: './model-year-form.component.html',
  styleUrls: ['./model-year-form.component.sass']
})
export class ModelYearFormComponent implements OnInit {

  fields: FormFields;

  constructor(
    private readonly modelService: ModelService,
    private readonly modelQuery: ModelQuery,
  ) { }

  ngOnInit() {
    this.modelService.fetchAll();
    this.fields = [
      {
        name: 'name',
        type: 'input',
        inputType: 'text',
        label: 'Display Name',
      },
      {
        name: 'description',
        type: 'input',
        inputType: 'textarea',
        label: 'Desription'
      },
      {
        name: 'modelId',
        type: 'browse',
        label: 'Model',
        displayedValueFieldName: 'modelName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.modelQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        }
      }
    ];
  }

}
