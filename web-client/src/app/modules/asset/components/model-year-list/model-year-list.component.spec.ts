import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelYearListComponent } from './model-year-list.component';

describe('ModelYearListComponent', () => {
  let component: ModelYearListComponent;
  let fixture: ComponentFixture<ModelYearListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelYearListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelYearListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
