import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModelYear } from '@modules/asset/models';

@Component({
  selector: 'app-model-year-list',
  templateUrl: './model-year-list.component.html',
  styleUrls: ['./model-year-list.component.sass']
})
export class ModelYearListComponent implements OnInit {

  columns = [
    { field: 'name', headerName: 'Display Name' },
    { field: 'description', headerName: 'Description' }
  ];

  @Input()
  modelYears: ModelYear[];
  @Output()
  rowSelected = new EventEmitter<ModelYear[]>();
  @Output()
  rowDoubleClicked = new EventEmitter<ModelYear>();
  @Output()
  bottomScroll = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
}
