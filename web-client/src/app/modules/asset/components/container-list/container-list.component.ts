import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Container } from '@modules/asset/models';

@Component({
  selector: 'app-container-list',
  templateUrl: './container-list.component.html',
  styleUrls: ['./container-list.component.sass']
})
export class ContainerListComponent implements OnInit {

  columns = [
    { field: 'name', headerName: 'Display Name' },
    { field: 'description', headerName: 'Description' }
  ];

  @Input()
  containers: Container[];
  @Output()
  rowSelected = new EventEmitter<Container[]>();
  @Output()
  rowDoubleClicked = new EventEmitter<Container>();
  @Output()
  bottomScroll = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
