import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisFormComponent } from './chassis-form.component';

describe('ChassisFormComponent', () => {
  let component: ChassisFormComponent;
  let fixture: ComponentFixture<ChassisFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
