import { Component, OnInit } from '@angular/core';
import { FormFields } from '@shell/models';
import { OrganizationQuery } from '@modules/party/state';
import { OrganizationService } from '@modules/party/services';

@Component({
  selector: 'app-chassis-form',
  templateUrl: './chassis-form.component.html',
  styleUrls: ['./chassis-form.component.sass']
})
export class ChassisFormComponent implements OnInit {

  fields: FormFields;

  constructor(
    private readonly organizationService: OrganizationService,
    private readonly organizationQuery: OrganizationQuery,
  ) { }

  ngOnInit() {
    this.organizationService.fetchAll();
    this.fields = [
      {
        name: 'name',
        type: 'input',
        inputType: 'textarea',
        label: 'Display Name',
      },
      {
        name: 'description',
        type: 'input',
        inputType: 'text',
        label: 'Display Name',
      },
      {
        name: 'ownerPartyId',
        type: 'browse',
        label: 'Owner',
        displayedValueFieldName: 'ownerName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.organizationQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        }
      },
      {
        name: 'plateNo',
        type: 'input',
        inputType: 'text',
        label: 'Plate No'
      },
      {
        name: 'chassisNo',
        type: 'input',
        inputType: 'text',
        label: 'Chassis No'
      },
      {
        name: 'axleQuantity',
        type: 'input',
        inputType: 'number',
        label: 'Axle Qty'
      },
      {
        name: 'statusTypeId',
        type: 'select',
        label: 'Status',
        options: [
          {
            name: 'Draft',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65b0'
          },
          {
            name: 'Available',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e0'
          },
          {
            name: 'Assigned',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e1'
          },
          {
            name: 'Running',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e2',
          },
          {
            name: 'Idle',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e3'
          },
          {
            name: 'Under Repair',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e4'
          },
          {
            name: 'Retired',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e5'
          },
          {
            name: 'Disposed',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65e6'
          }
        ]
      }
    ];
  }

}


