import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Model } from '@modules/asset/models';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.sass']
})
export class ModelListComponent implements OnInit {

  columns = [
    { field: 'name', headerName: 'Display Name' },
    { field: 'description', headerName: 'Description' }
  ];

  @Input()
  models: Model[];
  @Output()
  rowSelected = new EventEmitter<Model[]>();
  @Output()
  rowDoubleClicked = new EventEmitter<Model>();
  @Output()
  bottomScroll = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
}
