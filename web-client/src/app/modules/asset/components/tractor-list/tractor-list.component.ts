import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tractor } from '@modules/asset/models';

@Component({
  selector: 'app-tractor-list',
  templateUrl: './tractor-list.component.html',
  styleUrls: ['./tractor-list.component.sass']
})
export class TractorListComponent implements OnInit {
  columns = [
    { field: 'name', headerName: 'Display Name' },
    { field: 'description', headerName: 'Description' }
  ];

  @Input()
  tractors: Tractor[];
  @Output()
  rowSelected = new EventEmitter<Tractor[]>();
  @Output()
  rowDoubleClicked = new EventEmitter<Tractor>();
  @Output()
  bottomScroll = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
