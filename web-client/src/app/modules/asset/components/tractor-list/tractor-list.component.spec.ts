import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorListComponent } from './tractor-list.component';

describe('TractorListComponent', () => {
  let component: TractorListComponent;
  let fixture: ComponentFixture<TractorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
