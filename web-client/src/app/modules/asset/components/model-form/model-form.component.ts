import { Component, OnInit } from '@angular/core';
import { FormFields } from '@shell/models';
import { OrganizationService } from '@modules/party/services';
import { OrganizationQuery } from '@modules/party/state';


@Component({
  selector: 'app-model-form',
  templateUrl: './model-form.component.html',
  styleUrls: ['./model-form.component.sass']
})
export class ModelFormComponent implements OnInit {

  fields: FormFields;

  constructor(
    private readonly organizationService: OrganizationService,
    private readonly organizationQuery: OrganizationQuery,
  ) { }

  ngOnInit() {
    this.organizationService.fetchAll();
    this.fields = [
      {
        name: 'name',
        type: 'input',
        inputType: 'text',
        label: 'Display Name',
      },
      {
        name: 'description',
        type: 'input',
        inputType: 'textarea',
        label: 'Description'
      },
      {
        name: 'makerPartyId',
        type: 'browse',
        label: 'Maker',
        displayedValueFieldName: 'makerName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.organizationQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        }
      }
    ];
  }

}
