import { Component, OnInit } from '@angular/core';
import { FormFields } from '@shell/models';
import { OrganizationQuery } from '@modules/party/state';
import { OrganizationService } from '@modules/party/services';

@Component({
  selector: 'app-container-form',
  templateUrl: './container-form.component.html',
  styleUrls: ['./container-form.component.sass']
})
export class ContainerFormComponent implements OnInit {

  fields: FormFields;

  constructor(
    private readonly organizationService: OrganizationService,
    private readonly organizationQuery: OrganizationQuery,
  ) { }

  ngOnInit() {
    this.organizationService.fetchAll();
    this.fields = [
      {
        name: 'containerCode',
        type: 'input',
        inputType: 'text',
        label: 'Container Code',
      },
      {
        name: 'ownerPartyId',
        type: 'browse',
        label: 'Owner',
        displayedValueFieldName: 'ownerName',
        valueField: 'id',
        browseOptions: {
          labelField: 'name',
          rows: this.organizationQuery.selectAll(),
          columns: [
            { field: 'name', headerName: 'Display Name' },
            { field: 'description', headerName: 'Description' },
          ]
        }
      },
      {
        name: 'name',
        type: 'input',
        inputType: 'text',
        label: 'Display Name',
      },
      {
        name: 'description',
        type: 'textarea',
        label: 'Description',
      },
      {
        name: 'statusTypeId',
        type: 'select',
        label: 'Status',
        options: [
          {
            name: 'Draft',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65b0'
          },
          {
            name: 'Active',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65d0'
          },
          {
            name: 'Inactive',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65d2'
          },
          {
            name: 'Dissolved',
            value: '4fdf8b52-2260-4cc7-815f-c7d0405b65d1'
          }
        ]
      }
    ];
  }

}
