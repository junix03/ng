import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisListComponent } from './chassis-list.component';

describe('ChassisListComponent', () => {
  let component: ChassisListComponent;
  let fixture: ComponentFixture<ChassisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
