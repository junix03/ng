import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Chassis } from '@modules/asset/models';

@Component({
  selector: 'app-chassis-list',
  templateUrl: './chassis-list.component.html',
  styleUrls: ['./chassis-list.component.sass']
})
export class ChassisListComponent implements OnInit {

  columns = [
    { field: 'name', headerName: 'Display Name' },
    { field: 'description', headerName: 'Description' }
  ];

  @Input()
  chasseez: Chassis[];
  @Output()
  rowSelected = new EventEmitter<Chassis[]>();
  @Output()
  rowDoubleClicked = new EventEmitter<Chassis>();
  @Output()
  bottomScroll = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
