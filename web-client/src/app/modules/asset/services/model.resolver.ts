import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ModelService } from './model.service';
import { ModelStore } from '@modules/asset/state';

@Injectable({ providedIn: 'root' })
export class ModelResolver implements Resolve<boolean> {

  constructor(
    private modelService: ModelService,
    private modelStore: ModelStore,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = route.paramMap.get('id');
    return this.modelService.resolve(id).pipe(
      tap(model => this.modelStore.add(model)),
      map(() => true)
    );
  }
}
