import { Injectable } from '@angular/core';
import { EntityService } from '@modules/base/services';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ModelYearState, ModelYearStore } from '@modules/asset/state';
import { ModelYear } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ModelYearService extends EntityService<ModelYearState, ModelYear> {

  constructor(
    store: ModelYearStore,
    message: NzMessageService,
    router: Router,
    http: HttpClient
  ) {
    super('common-object/asset/modelyear', store, message, router, http);
  }

}
