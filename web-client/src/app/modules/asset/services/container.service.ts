import { Injectable } from '@angular/core';
import { EntityService } from '@modules/base/services';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ContainerState, ContainerStore } from '@modules/asset/state';
import { Container } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ContainerService extends EntityService<ContainerState, Container> {

  constructor(
    store: ContainerStore,
    message: NzMessageService,
    router: Router,
    http: HttpClient
  ) {
    super('common-object/asset/container', store, message, router, http);
  }

}
