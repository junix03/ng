import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ModelYearService } from './model-year.service';
import { ModelYearStore } from '@modules/asset/state';

@Injectable({ providedIn: 'root' })
export class ModelYearResolver implements Resolve<boolean> {

  constructor(
    private modelYearService: ModelYearService,
    private modelYearStore: ModelYearStore,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = route.paramMap.get('id');
    return this.modelYearService.resolve(id).pipe(
      tap(tractor => this.modelYearStore.add(tractor)),
      map(() => true)
    );
  }
}
