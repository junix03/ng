import { Injectable } from '@angular/core';
import { EntityService } from '@modules/base/services';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ChassisState, ChassisStore } from '@modules/asset/state';
import { Chassis } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ChassisService extends EntityService<ChassisState, Chassis> {

  constructor(
    store: ChassisStore,
    message: NzMessageService,
    router: Router,
    http: HttpClient
  ) {
    super('common-object/asset/chassis', store, message, router, http);
  }

}
