import { Injectable } from '@angular/core';
import { EntityService } from '@modules/base/services';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ModelState, ModelStore } from '@modules/asset/state';
import { Model } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class ModelService extends EntityService<ModelState, Model> {

  constructor(
    store: ModelStore,
    message: NzMessageService,
    router: Router,
    http: HttpClient
  ) {
    super('common-object/asset/model', store, message, router, http);
  }

}
