import { Injectable } from '@angular/core';
import { EntityService } from '@modules/base/services';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TractorState, TractorStore } from '@modules/asset/state';
import { Tractor } from '@modules/asset/models';

@Injectable({ providedIn: 'root' })
export class TractorService extends EntityService<TractorState, Tractor> {

  constructor(
    store: TractorStore,
    message: NzMessageService,
    router: Router,
    http: HttpClient
  ) {
    super('common-object/asset/tractor', store, message, router, http);
  }

}
