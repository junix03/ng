import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ContainerService } from './container.service';
import { ContainerStore } from '@modules/asset/state';

@Injectable({ providedIn: 'root' })
export class ContainerResolver implements Resolve<boolean> {

  constructor(
    private containerService: ContainerService,
    private containerStore: ContainerStore,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = route.paramMap.get('id');
    return this.containerService.resolve(id).pipe(
      tap(container => this.containerStore.add(container)),
      map(() => true)
    );
  }
}
