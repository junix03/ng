import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ChassisService } from './chassis.service';
import { ChassisStore } from '@modules/asset/state';

@Injectable({ providedIn: 'root' })
export class ChassisResolver implements Resolve<boolean> {

  constructor(
    private chassisService: ChassisService,
    private chassisStore: ChassisStore,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = route.paramMap.get('id');
    return this.chassisService.resolve(id).pipe(
      tap(chassis => this.chassisStore.add(chassis)),
      map(() => true)
    );
  }
}
