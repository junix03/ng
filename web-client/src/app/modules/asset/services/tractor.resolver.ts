import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { TractorService } from './tractor.service';
import { TractorStore } from '@modules/asset/state';

@Injectable({ providedIn: 'root' })
export class TractorResolver implements Resolve<boolean> {

  constructor(
    private tractorService: TractorService,
    private tractorStore: TractorStore,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const id = route.paramMap.get('id');
    return this.tractorService.resolve(id).pipe(
      tap(tractor => this.tractorStore.add(tractor)),
      map(() => true)
    );
  }
}
