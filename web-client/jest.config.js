module.exports = {
  'preset': 'jest-preset-angular',
  'setupTestFrameworkScriptFile': '<rootDir>/src/test.ts',
  'moduleNameMapper': {
    '^@auth(.*)': '<rootDir>/src/app/auth/$1',
    '^@shell(.*)': '<rootDir>/src/app/shell/$1',
    '^@modules(.*)': '<rootDir>/src/app/modules/$1',
    '^@environments(.*)': '<rootDir>/src/environments/$1'
  },
}
